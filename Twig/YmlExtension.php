<?php

namespace PiZone\AdminBundle\Twig;

use Symfony\Component\Yaml\Yaml;

class YmlExtension extends \Twig_Extension {

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('yml_dump', array($this, 'ymlDumpFilter')),
        );
    }

    public function ymlDumpFilter($data) {
        $data = json_decode($data, true);
        return Yaml::dump($data, 5);

    }

    public function getName() {
        return 'yml_dump';
    }

}
