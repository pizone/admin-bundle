<?php

namespace PiZone\AdminBundle\Twig;

use Symfony\Component\HttpFoundation\RequestStack;

class LocaleExtension extends \Twig_Extension {

    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $locale;
    protected $scriptName;

    public function __construct(RequestStack $requestStack, $container)
    {
        $this->container = $container;

        $request = $requestStack->getCurrentRequest();
        if($request) {
            $this->scriptName = $request->getScriptName();
            $this->locale = $request->getLocale();
        }
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('localeLink', array($this, 'localeFilter')),
        );
    }

    public function localeFilter($link, $prefix = "", $locale = null) {
        $kernel = $this->container->get('kernel');
        if(!$kernel->isDebug())
            $this->scriptName = '';
        if($locale)
            return $this->scriptName.'/'.$locale.$prefix.$link;
        if($this->locale != $this->container->getParameter('locale'))
            return $this->scriptName.'/'.$this->locale.$prefix.$link;

        return $this->scriptName.$prefix.$link;
    }

    public function getName() {
        return 'locale_extension';
    }

}
