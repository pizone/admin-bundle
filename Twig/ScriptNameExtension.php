<?php

namespace PiZone\AdminBundle\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;

/**
 * Twig extension for the Symfony HttpFoundation component.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class ScriptNameExtension extends \Twig_Extension
{
    protected $scriptName;
    protected $container;

    public function __construct(RequestStack $requestStack, $container)
    {
        $this->container = $container;

        $request = $requestStack->getCurrentRequest();
        if($request) {
            $this->scriptName = $request->getScriptName();
        }
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('scriptName', array($this, 'scriptName')),
        );
    }

    public function scriptName() {
        $kernel = $this->container->get('kernel');
        if(!$kernel->isDebug())
            $this->scriptName = '';

        return $this->scriptName;
    }

    public function getName() {
        return 'script_name_extension';
    }
}
