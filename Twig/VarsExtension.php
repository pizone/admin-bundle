<?php

namespace PiZone\AdminBundle\Twig;

use \Twig_Extension;

class VarsExtension extends Twig_Extension
{
    public function getName()
    {
        return 'vars_extension';
    }

    public function getFilters() {
        return array(
            'json_decode'   => new \Twig_Filter_Method($this, 'jsonDecode'),
        );
    }

    public function jsonDecode($str) {
        return json_decode($str);
    }
}