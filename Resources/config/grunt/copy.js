module.exports = {
    mainAdmin: {
        files: [
            {
                expand: true,
                cwd: 'web/bundles/pizoneadmin/font-awesome/fonts/',
                src: '**',
                dest: 'web/assetic/fonts/'
            },
            {
                expand: true,
                cwd: 'web/bundles/pizoneadmin/fonts/',
                src: '**',
                dest: 'web/assetic/fonts/'
            },
            {
                expand: true,
                cwd: 'web/bundles/pizoneadmin/css/patterns/',
                src: '**',
                dest: 'web/assetic/css/patterns/'
            }
        ]
    }
};
