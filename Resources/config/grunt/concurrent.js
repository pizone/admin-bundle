module.exports = {

    // Опции
    options: {
        limit: 3
    },

    // Задачи разработки
    devFirstAdmin: [
        'jshint'
    ],
    devSecondAdmin: [
        'cssmin',
        'uglify'
    ],
    devTherdAdmin: [
        'copy'
    ],

    // Производственные задачи
    prodFirstAdmin: [
        'jshint'
    ],
    prodSecondAdmin: [
        'cssmin',
        'uglify'
    ],
    prodTherdAdmin: [
        'copy'
    ]
};