module.exports = {
    options: {
        reporter: require('jshint-stylish')
    },
    main: [
        'web/bundles/pizoneadmin/js/conteroller/*.js',
        'web/bundles/pizoneadmin/js/directive/*.js',
        'web/bundles/pizoneadmin/js/model/*.js',
        'web/bundles/pizoneadmin/js/service/*.js',
        'web/bundles/pizoneadmin/js/translations/*.js',
        'web/bundles/pizoneadmin/js/config.js',
        'web/bundles/pizoneadmin/js/translations.js'
    ]
};