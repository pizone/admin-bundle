module.exports = {
    options: {
        shorthandCompacting: false,
        roundingPrecision: -1
    },
    target: {
        files: {
            'web/assetic/css/style.min.css': [
                'web/bundles/pizoneadmin/css/bootstrap.min.css',
                'web/bundles/pizoneadmin/css/bootstrap-datepicker.css',
                'web/bundles/pizoneadmin/font-awesome/css/font-awesome.css',
                'web/bundles/pizoneadmin/js/plugins/angucomplete-alt/angucomplete-alt.css',
                'web/bundles/pizoneadmin/css/animate.css',
                'web/bundles/pizoneadmin/css/style.css',
                'web/bundles/pizoneadmin/css/intis.css'
            ]
        }
    }
}