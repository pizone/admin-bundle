
function StateConfig($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $httpProvider, IdleProvider) {

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $urlRouterProvider.otherwise(function($injector){
        var $state = $injector.get("$state");
        var sService = $injector.get("SessionService");
        if(sService.CheckCredentials('ROLE_ADMIN'))
            $state.go('content.content_list');
        else if(sService.CheckCredentials('ROLE_MANAGER'))
            $state.go('shop.order_list');
        else
            $state.go('login');
    });

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider
        .state('error', {
            abstract: true,
            url: "",
            templateUrl:  "/bundles/pizoneadmin/tmpl/common/_admin_content.html"
        })
        .state('error.404', {
            url: "/404",
            templateUrl: "/bundles/pizoneadmin/tmpl/error/error404.html"
        })
        .state('error.500', {
            url: "/500",
            templateUrl: "/bundles/pizoneadmin/tmpl/error/error500.html"
        })
        .state('forbidden', {
            url: "/403",
            templateUrl: "/bundles/pizoneadmin/tmpl/error/error403.html",
            data: { specialClass: 'gray-bg' }
        })
    ;

    $httpProvider.interceptors.push('authInterceptor');
}
StateConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$httpProvider', 'IdleProvider', 'KeepaliveProvider'];

angular
    .module('PiZone.Admin')
    .config(StateConfig)
    .run(['$rootScope', '$state', '$stateParams', 'SessionService', function ($rootScope, $state, $stateParams, SessionService) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.user = null;

        // Здесь мы будем проверять авторизацию
        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                SessionService.CheckAccess(event, toState, toParams, fromState, fromParams);
            }
        );
    }
]);
