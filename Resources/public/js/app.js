(function () {
    angular.module('PiZone.Admin', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'pascalprecht.translate',       // Angular Translate
        'ngIdle',                       // Idle timer
        'ngSanitize',                    // ngSanitize
        'ngStorage',
        'ngCookies',
        'angucomplete-alt',
        'ui.tinymce'
    ]);
})();

// Other libraries are loaded dynamically in the config.js file using the library ocLazyLoad