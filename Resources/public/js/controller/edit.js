function EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    $scope.id = $stateParams.id;
    $scope.data = {};
    $scope.prefix = '';

    $scope.statelink ={
        list: '',
        'new': ''
    };
    $scope.fields = {};
    $scope.tabs = {};
    $scope.action = null;
    $scope._delete_token = '';
    $scope.formId = 'formId';
    $scope.pathToTmpl = PiZoneConfig.pathToTmpl;
    $scope.formData = new FormData();
    $scope.breadcrumbs = {
        title: '',
        param: '',
        path: [],
        active: ''
    };
    $scope.messages = {
        submit: {
            success: {
                title: 'COMMON.ALERT.SUBMIT.SUCCESS.TITLE',
                message: 'COMMON.ALERT.SUBMIT.SUCCESS.MESSAGE'
            },
            error: {
                title: 'COMMON.ALERT.SUBMIT.ERROR.TITLE',
                message: 'COMMON.ALERT.SUBMIT.ERROR.MESSAGE'
            }
        },
        remove: {
            confirm: {
                title: 'COMMON.ALERT.REMOVE.CONFIRM.TITLE',
                message: "COMMON.ALERT.REMOVE.CONFIRM.MESSAGE",
                confirmButton: "COMMON.ALERT.REMOVE.CONFIRM.CONFIRM_BUTTON",
                cancelButton: 'COMMON.ALERT.REMOVE.CONFIRM.CANCEL_BUTTON',
                cancelledTitle: 'COMMON.ALERT.REMOVE.CONFIRM.CANCELLED_TITLE',
                cancelledMessage: 'COMMON.ALERT.REMOVE.CONFIRM.CANCELLED_MESSAGE'
            },
            success: {
                title: 'COMMON.ALERT.REMOVE.SUCCESS.TITLE',
                message: 'COMMON.ALERT.REMOVE.SUCCESS.MESSAGE'
            },
            error: {
                title: 'COMMON.ALERT.REMOVE.ERROR.TITLE',
                message: 'COMMON.ALERT.REMOVE.ERROR.MESSAGE'
            }
        }
    };
    $scope.tinymceOptions = {
        plugins: [
            'advlist autolink lists link charmap print preview hr anchor pagebreak tinyvision',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'template paste textcolor colorpicker textpattern image imagetools'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media',
        content_css: [
            '/assetic/css/frontend.min.css'
        ],
        verify_html: false,
        image_advtab: true,
        relative_urls: false,
        tinyvision: {
            source: PiZoneConfig.apiPrefix + '/admin/media',
            upload: function(){
                var input = $(document.createElement('input'));
                input.attr("type", "file");
                input.trigger('click');

                input.change(function(){
                    var formData = new FormData();
                    formData.append('file', input[0].files[0]);

                    $http({
                        url: PiZoneConfig.apiPrefix + '/admin/media/upload',
                        method: 'POST',
                        data: formData,
                        headers: {'Content-Type': undefined}
                    }).then(
                        function(message){
                            var myFrame = $('.mce-window-body iframe');
                            $(myFrame[0].contentWindow.document).find('#refresh').click();
                        },
                        function(message){

                        });
                });
            }
        },
        external_plugins: {
            'tinyvision': '/bundles/pizoneadmin/js/plugins/tinyvision/build/plugin.js'
        }
    };
    $scope.Click = {
        ToList: ToList,
        ToNew: ToNew,
        Submit: Submit,
        SubmitAndList: SubmitAndList,
        SubmitAndNew: SubmitAndNew,
        Remove: Remove
    };
    $scope.Get = {
        Tabs: GetTabs,
        Fields: GetFields,
        Values: GetValues,
        Layout: GetLayout,
        Data: GetData
    };
    $scope.Befor = {
        ParseData: function(data){return data;},
        Submit: function(){}
    };
    $scope.Callback = {
        Init: function(){},
        Submit: {
            Success: CallbackSubmitSuccess,
            Error: CallbackSubmitError
        },
        Remove: {
            Success: CallbackRemoveSuccess,
            Error: CallbackRemoveError
        }
    };
    $scope.Init = Init;
    $scope.ParseData = ParseData;
    $scope.ViewNotify = ViewNotify;
    $scope.IsValid = IsValid;
    $scope.prepareToAutocomplete = prepareToAutocomplete;

    function Init(){
        $scope.Get.Layout(function(){
            $scope.Callback.Init();
        });
    }

    function ToList(){
        $state.go($scope.statelink.list);
    }

    function ToNew(){
        $state.go($scope.statelink.new);
    }

    function Save(callback){
        if($scope.IsValid()) {
            var container = $('#' + $scope.formId);
            var loader = new Loader(container);
            $scope.Befor.Submit();
            if(loader.ready === false)
                loader.Create();

            $http({
                url: $scope.action,
                method: 'POST',
                data: $scope.Get.Values(),
                headers: {'Content-Type': undefined}
            }).then(
                function(message){
                    message = JSON.parse(message.data);
                    callback(message);
                    loader.Delete();
                },
                function(message){
                    if (message.status == 400) {
                        message = JSON.parse(message.data);

                        console.log(message);
                        $timeout(function() {
                            ParseData(message);
                        });
                    }
                    else
                        $scope.ViewNotify('submit', 'error');
                    loader.Delete();
                });
        }
    }


    function Submit(){
        Save(function(message){
            if(message.result == 'ok') {
                $scope.Callback.Submit.Success(message);
            }
            else
                $scope.Callback.Submit.Error(message);
        });
    }

    function SubmitAndList(){
        Save(function(message){
            if(message.result == 'ok') {
                $scope.Click.ToList();
                $scope.Callback.Submit.Success(message);
            }
            else
                $scope.Callback.Submit.Error(message);
        });
    }

    function SubmitAndNew(){
        Save(function(message){
            if(message.result == 'ok') {
                $scope.Click.ToNew();
                $scope.Callback.Submit.Success(message);
            }
            else
                $scope.Callback.Submit.Error(message);
        });
    }

    function IsValid(){
        var validate = new Validators();
        if($scope.fields)
            ValidateFields($scope.fields, validate);
        if($scope.tabs){
            $.each($scope.tabs, function(i){
                if($scope.tabs[i].groups) {
                    $.each($scope.tabs[i].groups, function (j) {
                        $.each($scope.tabs[i].groups[j], function (k) {
                            if ($scope.tabs[i].groups[j][k].assert)
                                validate.checkField($scope.tabs[i].groups[j][k]);
                            if(!$scope.tabs[i].groups[j][k].field.valid)
                                $scope.tabs[i].valid = false;
                            if(!$scope.tabs[i].groups[j][k].hasOwnProperty('options') ||
                                ($scope.tabs[i].groups[j][k].options.hasOwnProperty('validate') && $scope.tabs[i].groups[j][k].options.validate)){

                                if ($scope.tabs[i].groups[j][k].field && $scope.tabs[i].groups[j][k].field.form && Object.keys($scope.tabs[i].groups[j][k].field.form).length > 0) {
                                    if ($scope.tabs[i].groups[j][k].field.type == 'choice') {
                                        ValidateFields([$scope.tabs[i].groups[j][k].field.form], validate, $scope.tabs[i]);
                                    }
                                    else {
                                        $.each($scope.tabs[i].groups[j][k].field.form, function (l) {
                                            ValidateFields($scope.tabs[i].groups[j][k].field.form[l], validate, $scope.tabs[i]);
                                        });
                                    }
                                }
                            }
                        });
                    });
                }
            });
        }
        if(!validate.isValid())
            $scope.ViewNotify('submit', 'error');
        return validate.isValid();
    }

    function ValidateFields(fields, validate, tab, type){
        if(fields.hasOwnProperty('form')) {
            ValidateFields(fields.form, validate, tab, type);
        }
        else {
            $.each(fields, function (j) {
                $.each(fields[j], function (k) {
                    if (fields[j][k].assert) {
                        validate.checkField(fields[j][k]);
                        if (!fields[j][k].field.valid && tab)
                            tab.valid = false;
                    }
                    if (fields[j][k].field && fields[j][k].field.form && Object.keys(fields[j][k].field.form).length > 0) {
                        $.each(fields[j][k].field.form, function (l) {
                            ValidateFields(fields[j][k].field.form[l], validate);
                        });
                    }
                });
            });
        }
    }

    function ParseData(message){
        message.fields = $scope.Befor.ParseData(message.fields);
        var data = DataProcessing.Normalize(message.fields);
        $scope.data = message;
        $scope.Get.Tabs(data);
        $scope.Get.Fields(data);
        $scope.action = message.action;
        $scope._delete_token = message._delete_token;
    }

    function GetValues(){
        var data =  $('#' + $scope.formId).serializeArray();

        for (var i = 0; i <= data.length - 1; i++) {
            if(!$scope.formData.has(data[i].name))
                $scope.formData.append(data[i].name, data[i].value);
            else
                $scope.formData.set(data[i].name, data[i].value);
        }

        return $scope.formData;
    }

    function GetLayout(callback){
        var container = $('#' + $scope.formId);
        var loader = new Loader(container);
        loader.Create();
        $http({
            url: PiZoneConfig.apiPrefix + '/' + $scope.prefix + '/' + $scope.id + '/edit',
            method: 'GET'
        }).then(
            function(message){
                $scope.Get.Data(message, callback);
                loader.Delete();
            },
            function(message){
                if (message.status == 400) {
                    $scope.Get.Data(message, callback);
                }
                loader.Delete();
            });
    }

    function GetData(message, callback){
        message = JSON.parse(message.data);
        $scope.ParseData(message);
        if(callback)
            callback();
    }

    function GetFields(data){
        $scope.fields = [];
    }

    function GetTabs(data){
        $scope.tabs = [];
    }

    function CallbackSubmitSuccess(message){
        ViewNotify('submit', 'success');
    }

    function CallbackSubmitError(message){
        $timeout(function() {
            $scope.ParseData(message);
        });
        ViewNotify('submit', 'error');
    }

    function CallbackRemoveSuccess(message){
        ViewNotify('remove', 'success');
        $scope.Click.ToList();
    }

    function CallbackRemoveError(message){
        ViewNotify('remove', 'error');
    }

    function Remove(){
        function translate(key){
            return $filter('translate')($scope.messages.remove.confirm[key]);
        }
        SweetAlert.swal({
                title: translate('title'),
                text: translate('message'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: translate('confirmButton'),
                cancelButtonText: translate('cancelButton'),
                closeOnConfirm: false,
                closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    ConfirmDelete();
                } else {
                    SweetAlert.swal(translate('cancelledTitle'), translate('cancelledMessage'), "error");
                }
            });
    }

    function ConfirmDelete() {
        var url = PiZoneConfig.apiPrefix + '/' + $scope.prefix + '/' + $scope.id + '/delete';

        var container = $('#' + $scope.formId);
        var loader = new Loader(container);
        loader.Create();
        $http({
            url: url,
            method: 'POST',
            data: {_token: $scope._delete_token},
        }).then(
            function(message){
                message = message.data;
                if (message.result == 'ok')
                    $scope.Callback.Remove.Success(message);
                else
                    $scope.Callback.Remove.Error(message);
                loader.Delete();
            },
            function(message){
                message = message.data;
                if (message.status == 404)
                    $scope.messages.remove.error.message = $filter('translate')('COMMON.ALERT.REMOVE.ERROR.ERROR_404', {id: $scope.id});
                if (message.status == 400)
                    $scope.messages.remove.error.message = 'COMMON.ALERT.REMOVE.ERROR.ERROR_400';
                if (message.status == 500)
                    $scope.messages.remove.error.message = 'COMMON.ALERT.REMOVE.ERROR.ERROR_500';
                $scope.Callback.Remove.Error();
                loader.Delete();
            });
    }

    function ViewNotify(action, res){
        function translate(key){
            return $filter('translate')($scope.messages[action][res][key]);
        }
        SweetAlert.swal(translate('title'), translate('message'), res);
    }

    function prepareToAutocomplete(field, prefix){
        field.template = FieldDispatcher.GetLayout('autocomplete');
        field.url = PiZoneConfig.apiPrefix + '/' + prefix + '/find?str=';
        if(field.attr && field.attr.model){
            field.init = field.attr.model;
        }
        field.onSelected = function(data){
            var value = '';
            if(data){
                value = data.originalObject.id;
            }
            field.value = value;
        };
        field.onFocusOut = function(){
            if(!field.value)
                $scope.$broadcast('angucomplete-alt:clearInput', field.id);
        };
    }
}
EditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];