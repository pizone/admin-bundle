function NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    $scope.prefix = '';

    $scope.statelink ={
        list: '',
        'new': '',
        edit: ''
    };
    $scope.data = {};
    $scope.fields = {};
    $scope.tabs = {};
    $scope.action = null;
    $scope.formId = 'formId';
    $scope.pathToTmpl = PiZoneConfig.pathToTmpl;
    $scope.formData = new FormData();
    $scope.breadcrumbs = {
        title: '',
        path: [],
        active: ''
    };
    $scope.messages = {
        submit: {
            success: {
                title: 'COMMON.ALERT.SUBMIT.SUCCESS.TITLE',
                message: 'COMMON.ALERT.SUBMIT.SUCCESS.MESSAGE'
            },
            error: {
                title: 'COMMON.ALERT.SUBMIT.ERROR.TITLE',
                message: 'COMMON.ALERT.SUBMIT.ERROR.MESSAGE'
            }
        },
        remove: {
            confirm: {
                title: 'COMMON.ALERT.REMOVE.CONFIRM.TITLE',
                message: "COMMON.ALERT.REMOVE.CONFIRM.MESSAGE",
                confirmButton: "COMMON.ALERT.REMOVE.CONFIRM.CONFIRM_BUTTON",
                cancelButton: 'COMMON.ALERT.REMOVE.CONFIRM.CANCEL_BUTTON',
                cancelledTitle: 'COMMON.ALERT.REMOVE.CONFIRM.CANCELLED_TITLE',
                cancelledMessage: 'COMMON.ALERT.REMOVE.CONFIRM.CANCELLED_MESSAGE'
            },
            success: {
                title: 'COMMON.ALERT.REMOVE.SUCCESS.TITLE',
                message: 'COMMON.ALERT.REMOVE.SUCCESS.MESSAGE'
            },
            error: {
                title: 'COMMON.ALERT.REMOVE.ERROR.TITLE',
                message: 'COMMON.ALERT.REMOVE.ERROR.MESSAGE'
            }
        }
    };
    $scope.Click = {
        ToList: ToList,
        ToNew: ToNew,
        ToEdit: ToEdit,
        Submit: Submit,
        SubmitAndList: SubmitAndList,
        SubmitAndNew: SubmitAndNew,
    };
    $scope.Get = {
        Tabs: GetTabs,
        Fields: GetFields,
        Values: GetValues,
        Layout: GetLayout,
        Data: GetData
    };
    $scope.Befor = {
        ParseData: function(data){return data;},
        Submit: function(){}
    };
    $scope.Callback = {
        Init: function(){},
        Success: CallbackSuccess,
        Error: CallbackError
    };
    $scope.Init = Init;
    $scope.IsValid = IsValid;
    $scope.ParseData = ParseData;
    $scope.ViewNotify = ViewNotify;
    $scope.prepareToAutocomplete = prepareToAutocomplete;
    $scope.GetBaseQuery = GetBaseQuery;

    function Init(){
        $scope.Get.Layout(function(){
            $scope.Callback.Init();
            EventDispatcher.DispatchEvent('change-route', $scope.breadcrumbs);
        });
    }

    function ToList(){
        $state.go($scope.statelink.list);
    }

    function ToNew(){
        $state.go($scope.statelink.new);
    }

    function ToEdit(id){
        $state.go($scope.statelink.edit, {id: id});
    }

    function Save(callback){
        if($scope.IsValid()) {
            var container = $('#' + $scope.formId);
            var loader = new Loader(container);
            $scope.Befor.Submit();
            if (loader.ready === false)
                loader.Create();

            $http({
                url: $scope.action,
                method: 'POST',
                data: $scope.Get.Values(),
                headers: {'Content-Type': undefined}
            }).then(
                function(message){
                    message = JSON.parse(message.data);
                    callback(message);
                    loader.Delete();
                },
                function(message){
                    loader.Delete();
                    if (message.status == 400) {
                        message = JSON.parse(message.data);
                        $timeout(function() {
                            $scope.Callback.Error(message);
                        });
                    }
                    else
                        $scope.ViewNotify('submit', 'error');
                });
        }
    }

    function Submit(){
        Save(function(message){
            if(message.result == 'ok') {
                $scope.Click.ToEdit(message.id);
                $scope.Callback.Success(message);
            }
            else
                $scope.Callback.Error(message);
        });
    }

    function SubmitAndList(){
        Save(function(message){
            if(message.result == 'ok') {
                $scope.Click.ToList();
                $scope.Callback.Success(message);
            }
            else
                $scope.Callback.Error(message);
        });
    }

    function SubmitAndNew(){
        Save(function(message){
            if(message.result == 'ok') {
                $scope.Click.ToNew();
                $scope.Callback.Success(message);
            }
            else
                $scope.Callback.Error(message);
        });
    }


    function IsValid(){
        var validate = new Validators();
        if($scope.fields)
            ValidateFields($scope.fields, validate);
        if($scope.tabs){
            $.each($scope.tabs, function(i){
                if($scope.tabs[i].groups) {
                    $.each($scope.tabs[i].groups, function (j) {
                        $.each($scope.tabs[i].groups[j], function (k) {
                            if ($scope.tabs[i].groups[j][k].assert)
                                validate.checkField($scope.tabs[i].groups[j][k]);
                            if(!$scope.tabs[i].groups[j][k].field.valid)
                                $scope.tabs[i].valid = false;
                            if(!$scope.tabs[i].groups[j][k].hasOwnProperty('options') ||
                                ($scope.tabs[i].groups[j][k].options.hasOwnProperty('validate') && $scope.tabs[i].groups[j][k].options.validate)){

                                if ($scope.tabs[i].groups[j][k].field && $scope.tabs[i].groups[j][k].field.form && Object.keys($scope.tabs[i].groups[j][k].field.form).length > 0) {
                                    if ($scope.tabs[i].groups[j][k].field.type == 'choice') {
                                        ValidateFields([$scope.tabs[i].groups[j][k].field.form], validate, $scope.tabs[i]);
                                    }
                                    else {
                                        $.each($scope.tabs[i].groups[j][k].field.form, function (l) {
                                            ValidateFields($scope.tabs[i].groups[j][k].field.form[l], validate, $scope.tabs[i]);
                                        });
                                    }
                                }
                            }
                        });
                    });
                }
            });
        }
        if(!validate.isValid())
            $scope.ViewNotify('submit', 'error');
        return validate.isValid();
    }

    function ValidateFields(fields, validate, tab, type){
        if(fields.hasOwnProperty('form')) {
            ValidateFields(fields.form, validate, tab, type);
        }
        else {
            $.each(fields, function (j) {
                $.each(fields[j], function (k) {
                    if (fields[j][k].assert) {
                        validate.checkField(fields[j][k]);
                        if (!fields[j][k].field.valid && tab)
                            tab.valid = false;
                    }
                    if (fields[j][k].field && fields[j][k].field.form && Object.keys(fields[j][k].field.form).length > 0) {
                        $.each(fields[j][k].field.form, function (l) {
                            ValidateFields(fields[j][k].field.form[l], validate);
                        });
                    }
                });
            });
        }
    }

    function ViewNotify(action, res){
        function translate(key){
            return $filter('translate')($scope.messages[action][res][key]);
        }
        SweetAlert.swal(translate('title'), translate('message'), res);
    }

    function ParseData(message){
        message.fields = $scope.Befor.ParseData(message.fields);
        var data = DataProcessing.Normalize(message.fields);
        $scope.data = message;
        $scope.Get.Tabs(data);
        $scope.Get.Fields(data);
        $scope.action = message.action;
    }

    function GetValues(){
        var data =  $('#' + $scope.formId).serializeArray();

        for (var i = 0; i <= data.length - 1; i++) {
            if(!$scope.formData.has(data[i].name))
                $scope.formData.append(data[i].name, data[i].value);
            else
                $scope.formData.set(data[i].name, data[i].value);
        }

        return $scope.formData;
    }

    function GetLayout(callback){
        var container = $('#' + $scope.formId);
        var loader = new Loader(container);
        loader.Create();

        $http({
            url: $scope.GetBaseQuery(),
            method: 'GET'
        }).then(
            function(message){
                $scope.Get.Data(message, callback);
                loader.Delete();
            },
            function(message){
                if (message.status == 400) {
                    $scope.Get.Data(message, callback);
                }
                loader.Delete();
            });
    }

    function GetBaseQuery(){
        return PiZoneConfig.apiPrefix + '/' + $scope.prefix + '/new';
    }

    function GetData(message, callback){
        message = JSON.parse(message.data);
        $scope.ParseData(message);
        if(callback)
            callback();
    }

    function GetFields(data){
        $scope.fields = [];
    }

    function GetTabs(data){
        $scope.tabs = [];
    }

    function CallbackSuccess(message){
        $scope.ViewNotify('submit', 'success');
    }

    function CallbackError(message){
        $scope.ViewNotify('submit', 'error');
        $timeout(function(){
            $scope.ParseData(message);
        });
    }

    function prepareToAutocomplete(field, prefix){
        field.template = FieldDispatcher.GetLayout('autocomplete');
        field.url = PiZoneConfig.apiPrefix + '/' + prefix + '/find?str=';
        if(field.attr && field.attr.model){
            field.init = field.attr.model;
        }
        field.onSelected = function(data){
            var value = '';
            if(data){
                value = data.originalObject.id;
            }
            field.value = value;
        };
        field.onFocusOut = function(){
            if(!field.value)
                $scope.$broadcast('angucomplete-alt:clearInput', field.id);
        };
    }
}
NewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];