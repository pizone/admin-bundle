function TranslateCtrl($translate, $scope) {
    $scope.useTranslate = '';
    $scope.ruFlag = PiZoneConfig.pathToImg + 'flags/16/Russia.png';
    $scope.enFlag = PiZoneConfig.pathToImg + 'flags/16/United-Kingdom.png';
    $scope.changeLanguage = function (langKey) {
        $translate.use(langKey);
        $scope.language = langKey;
        getFlag(langKey);
    };

    function Init(){
        var trans = $translate.use();
        getFlag(trans);
    }

    function getFlag(trans){
        if(trans == 'ru')
            $scope.useTranslate = $scope.ruFlag;
        else
            $scope.useTranslate = $scope.enFlag;
    }

    Init();
}
TranslateCtrl.$inject = ['$translate', '$scope'];
angular
    .module('PiZone.Admin')
    .controller('translateCtrl', TranslateCtrl);