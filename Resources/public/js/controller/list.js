function ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    $scope.prefix = '';
    $scope.model = {};
    $scope.pageId = $stateParams.pageId;
    $scope.formFilterId = '';
    $scope.listId = 'listId';
    $scope.isAllSelected = false;
    $scope.breadcrumbs = {
        title: '',
        path: [],
        active: ''
    };
    $scope.order = {
        sort: $stateParams.sort,
        order_by: $stateParams.order_by
    };
    $scope.statelink = {
        add: '',
        page: '',
        edit: ''
    };
    $scope._token = {
        filter: '',
        batch: ''
    };
    $scope.perPage = 10;
    $scope.pager = {};
    $scope.filters = {};
    $scope.list = [];
    $scope.data = {};
    $scope.showColumns = {
        all: []
    };
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4]
    };
    $scope.pathToTmpl = PiZoneConfig.pathToTmpl;
    $scope.pathToCatalogImg = PiZoneConfig.pathToCatalogImg;
    $scope.breadcrumbs = [];
    self.messages = {
        remove: {
            confirm: {
                title: 'COMMON.ALERT.BATCH.REMOVE.CONFIRM.TITLE',
                message: "COMMON.ALERT.BATCH.REMOVE.CONFIRM.MESSAGE",
                confirmButton: "COMMON.ALERT.BATCH.REMOVE.CONFIRM.CONFIRM_BUTTON",
                cancelButton: 'COMMON.ALERT.BATCH.REMOVE.CONFIRM.CANCEL_BUTTON',
                cancelledTitle: 'COMMON.ALERT.BATCH.REMOVE.CONFIRM.CANCELLED_TITLE',
                cancelledMessage: 'COMMON.ALERT.BATCH.REMOVE.CONFIRM.CANCELLED_MESSAGE'
            },
            success: {
                title: 'COMMON.ALERT.BATCH.REMOVE.SUCCESS.TITLE',
                message: 'COMMON.ALERT.BATCH.REMOVE.SUCCESS.MESSAGE'
            },
            error: {
                title: 'COMMON.ALERT.BATCH.REMOVE.ERROR.TITLE',
                message: 'COMMON.ALERT.BATCH.REMOVE.ERROR.MESSAGE'
            }
        },
        active: {
            confirm: {
                title: 'COMMON.ALERT.BATCH.ACTIVE.CONFIRM.TITLE',
                message: "COMMON.ALERT.BATCH.ACTIVE.CONFIRM.MESSAGE",
                confirmButton: "COMMON.ALERT.BATCH.ACTIVE.CONFIRM.CONFIRM_BUTTON",
                cancelButton: 'COMMON.ALERT.BATCH.ACTIVE.CONFIRM.CANCEL_BUTTON',
                cancelledTitle: 'COMMON.ALERT.BATCH.ACTIVE.CONFIRM.CANCELLED_TITLE',
                cancelledMessage: 'COMMON.ALERT.BATCH.ACTIVE.CONFIRM.CANCELLED_MESSAGE'
            },
            success: {
                title: 'COMMON.ALERT.BATCH.ACTIVE.SUCCESS.TITLE',
                message: 'COMMON.ALERT.BATCH.ACTIVE.SUCCESS.MESSAGE'
            },
            error: {
                title: 'COMMON.ALERT.BATCH.ACTIVE.ERROR.TITLE',
                message: 'COMMON.ALERT.BATCH.ACTIVE.ERROR.MESSAGE'
            }
        },
        inactive: {
            confirm: {
                title: 'COMMON.ALERT.BATCH.INACTIVE.CONFIRM.TITLE',
                message: "COMMON.ALERT.BATCH.INACTIVE.CONFIRM.MESSAGE",
                confirmButton: "COMMON.ALERT.BATCH.INACTIVE.CONFIRM.CONFIRM_BUTTON",
                cancelButton: 'COMMON.ALERT.BATCH.INACTIVE.CONFIRM.CANCEL_BUTTON',
                cancelledTitle: 'COMMON.ALERT.BATCH.INACTIVE.CONFIRM.CANCELLED_TITLE',
                cancelledMessage: 'COMMON.ALERT.BATCH.INACTIVE.CONFIRM.CANCELLED_MESSAGE'
            },
            success: {
                title: 'COMMON.ALERT.BATCH.INACTIVE.SUCCESS.TITLE',
                message: 'COMMON.ALERT.BATCH.INACTIVE.SUCCESS.MESSAGE'
            },
            error: {
                title: 'COMMON.ALERT.BATCH.INACTIVE.ERROR.TITLE',
                message: 'COMMON.ALERT.BATCH.INACTIVE.ERROR.MESSAGE'
            }
        }
    };
    $scope.Click = {
        Add: ClickAdd,
        Page: ClickPage,
        ChangePerPage: ChangePerPage,
        Filters: ClickFilters,
        Edit: ClickEdit,
        Reset: ClickReset,
        AddFilter: ClickAddFilter,
        ShowAllFields: ClickShowAllFields,
        HideAllFields: ClickHideAllFields,
        HideFilter: ClickHideFilter,
        ToggleAll: ClickToggleAll,
        OptionToggled: ClickOptionToggled,
        Batch: {
            Remove: ClickBatchRemove,
            Active: ClickBatchActive,
            Inactive: ClickBatchInactive
        }
    };
    $scope.Callback = {
        Init: function(){},
        GetData: function(){},
        GetList: CallbackGetList,
        Batch: {
            Delete: {
                Success: CallbackDeleteSuccess,
                Error: function(){}
            },
            Active: {
                Success: CallbackActiveSuccess,
                Error: function(){}
            },
            Inactive: {
                Success: CallbackInactiveSuccess,
                Error: function(){}
            }
        }
    };
    $scope.Get = {
        Layout: GetLayout,
        Data: ParseData,
        Selected: GetSelected
    };
    $scope.Init = Init;
    $scope.CheckUseFilter = CheckUseFilter;
    $scope.ShowColumns = ShowColumns;
    $scope.GetBaseQuery = GetBaseQuery;
    $scope.ShowDateFilter = ShowDateFilter;
    $scope.CheckPerPage = CheckPerPage;
    $scope.prepareToAutocomplete = prepareToAutocomplete;
    $scope.ViewNotify = ViewNotify;
    $scope.ConfirmDelete = ConfirmDelete;
    $scope.ConfirmActive = ConfirmActive;
    $scope.ConfirmInactive = ConfirmInactive;

    function Init(){
        var perPage = $cookieStore.get('intisweb_' + $scope.prefix + '_max_per_page');
        if(perPage)
            $scope.perPage = parseInt(perPage);

        $scope.Get.Layout();

        EventDispatcher.AddEventListener('update_page_' + $scope.prefix, function(){
            $timeout(function(){
                $scope.Get.Layout();
            });
        });
        var prefixForm = $scope.prefix.replace('/', '_');
        $scope.formFilterId = prefixForm + '_filterForm';

        $scope.Callback.Init();
    }

    function ClickAddFilter(block){
        block.show =  true;
        if(block.field.type == 'date_range') {
            block.field.form.from.value = '';
            block.field.form.to.value = '';
        }
        else
            block.field.value = '';

        var test = false;
        $.each($scope.filters, function(i){
            if($scope.filters[i].show)
                test = true;
        });

        if(!test)
            $scope.filters[0].show = true;
    }

    function CheckPerPage(i){
        if(i == $scope.perPage)
            return true;
        return false;
    }

    function ClickHideFilter(block){
        block.show = false;
    }

    function ClickShowAllFields(){
        $.each($scope.filters, function(i){
            $scope.filters[i].show = true;
        });
    }

    function ClickHideAllFields(){
        $.each($scope.filters, function(i){
            $scope.filters[i].show = false;
        });
        $scope.filters[0].show = true;
    }

    function ClickAdd(){
        $state.go($scope.statelink.add);
    }

    function ClickPage(page){
        $state.go($scope.statelink.page, {pageId: page});
    }

    function ClickEdit(id) {
        $state.go($scope.statelink.edit, {id: id});
    }

    function ClickToggleAll(){
        var toggleStatus = $scope.isAllSelected;
        angular.forEach($scope.list, function(itm){ itm.selected = toggleStatus; });
    }

    function ClickOptionToggled(){
        $scope.isAllSelected = $scope.list.every(function(itm){ return itm.selected; });
    }

    function translate(action, key) {
        return $filter('translate')(self.messages[action].confirm[key]);
    }

    function AlertBatchActions(action, callback){
        var selected = $scope.Get.Selected();
        var ids = [];
        $.each(selected, function(i){
            ids.push(selected[i].id);
        });

        if(ids.length > 0) {
            SweetAlert.swal({
                    title: translate(action, 'title'),
                    text: translate(action, 'message'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: translate(action, 'confirmButton'),
                    cancelButtonText: translate(action, 'cancelButton'),
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        callback(ids);
                    } else {
                        SweetAlert.swal(translate(action, 'cancelledTitle'), translate(action, 'cancelledMessage'), "error");
                    }
                });
        }
        else
            $scope.ViewNotify('batch', action, 'error');
    }

    function ClickBatchRemove(){
        AlertBatchActions('remove', function(ids){
            $scope.ConfirmDelete(ids);
        });
    }

    function ClickBatchActive(){
        AlertBatchActions('active', function(ids){
            $scope.ConfirmActive(ids);
        });
    }

    function ClickBatchInactive(){
        AlertBatchActions('inactive', function(ids){
            $scope.ConfirmInactive(ids);
        });
    }

    function ConfirmDelete(ids){
        var url = PiZoneConfig.apiPrefix + '/' + $scope.prefix + '/batch/delete';

        $http({
            url: url,
            method: 'POST',
            data: JSON.stringify({_token: $scope._token.batch.delete, ids: ids}),
            headers: {'Content-Type': 'application/json'}
        }).then(
            function(message){
                message = message.data;
                if (message.result == 'ok') {
                    ViewNotify('remove', 'success');
                    $scope.Callback.Batch.Delete.Success(message);
                }
                else {
                    ViewNotify('remove', 'error');
                    $scope.Callback.Batch.Delete.Error(message);
                }
            },
            function(message){
                message = message.data;
                if (message.status == 400)
                    self.messages.remove.error.message = 'COMMON.ALERT.BATCH.REMOVE.ERROR.ERROR_400';
                if (message.status == 500)
                    self.messages.remove.error.message = 'COMMON.ALERT.BATCH.REMOVE.ERROR.ERROR_500';
                $scope.Callback.Batch.Delete.Error();
                ViewNotify('remove', 'error');
            });
    }

    function ConfirmActive(ids){
        var url = PiZoneConfig.apiPrefix + '/' + $scope.prefix + '/batch/active';

        $http({
            url: url,
            method: 'POST',
            data: JSON.stringify({_token: $scope._token.batch.active, ids: ids, active: true}),
            headers: {'Content-Type': 'application/json'}
        }).then(
            function(message){
                message = message.data;
                if (message.result == 'ok') {
                    ViewNotify('active', 'success');
                    $scope.Callback.Batch.Active.Success(message);
                }
                else {
                    ViewNotify('active', 'error');
                    $scope.Callback.Batch.Active.Error(message);
                }
            },
            function(message){
                message = message.data;
                if (message.status == 400)
                    self.messages.active.error.message = 'COMMON.ALERT.BATCH.ACTIVE.ERROR.ERROR_400';
                if (message.status == 500)
                    self.messages.active.error.message = 'COMMON.ALERT.BATCH.ACTIVE.ERROR.ERROR_500';
                $scope.Callback.Batch.Active.Error();
                ViewNotify('active', 'error');
            });
    }

    function ConfirmInactive(ids){
        var url = PiZoneConfig.apiPrefix + '/' + $scope.prefix + '/batch/active';

        $http({
            url: url,
            method: 'POST',
            data: JSON.stringify({_token: $scope._token.batch.active, ids: ids, active: false}),
            headers: {'Content-Type': 'application/json'}
        }).then(
            function(message){
                message = message.data;
                if (message.result == 'ok') {
                    ViewNotify('inactive', 'success');
                    $scope.Callback.Batch.Inactive.Success(message);
                }
                else {
                    ViewNotify('inactive', 'error');
                    $scope.Callback.Batch.Inactive.Error(message);
                }
            },
            function(message){
                message = message.data;
                if (message.status == 400)
                    self.messages.active.error.message = 'COMMON.ALERT.BATCH.INACTIVE.ERROR.ERROR_400';
                if (message.status == 500)
                    self.messages.active.error.message = 'COMMON.ALERT.BATCH.INACTIVE.ERROR.ERROR_500';
                $scope.Callback.Batch.Inactive.Error();
                ViewNotify('inactive', 'error');
            });
    }

    function GetSelected() {
        return $scope.list.filter(function(item){
            return item.selected === true;
        });
    }

    function CallbackDeleteSuccess(data){
        $scope.Get.Layout();
    }

    function CallbackActiveSuccess(data){
        $scope.Get.Layout();
    }

    function CallbackInactiveSuccess(data){
        $scope.Get.Layout();
    }

    function ChangePerPage(perPage){
        if(perPage != $scope.perPage) {
            $cookieStore.put('intisweb_' + $scope.prefix + '_max_per_page', perPage);
            $scope.perPage = perPage;
            $state.go($scope.statelink.page, {pageId: 1, perPage: perPage});
        }
    }

    function GetBaseQuery(){
        return PiZoneConfig.apiPrefix + '/' + $scope.prefix + '';
    }

    function GetLayout(){
        var query = $scope.GetBaseQuery();
        var params = [];
        if($scope.pageId)
            params.push('page=' + $scope.pageId);
        if($scope.perPage)
            params.push('perPage=' + $scope.perPage);
        if($scope.order.sort && $scope.order.order_by)
            params.push('sort=' + $scope.order.sort + '&order_by=' + $scope.order.order_by);
        if(params.length > 0) {
            if(/\?/.test(query))
                query = query + '&' + params.join('&');
            else
                query = query + '?' + params.join('&');
        }
        $scope.list = [];
        var container = $('#' + $scope.listId);
        var loader = new Loader(container);
        loader.Create();
        $http({
            url: query,
            method: 'POST',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(
            function(response){
                response = JSON.parse(response.data);
                $scope.Callback.GetList(response);
                loader.Delete();
            },
            function(response){
                if (response.status == 400) {
                    response = JSON.parse(response.data);
                    $scope.Callback.GetList(response);
                }
                loader.Delete();
            });
    }

    function ClickFilters(){
        var container = $('#' + $scope.listId);
        var loader = new Loader(container);
        loader.Create();
        $http({
            url: $scope.GetBaseQuery() + '/filter',
            method: 'POST',
            data: $('#' + $scope.formFilterId).serialize(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(
            function(response){
                response = JSON.parse(response.data);
                $scope.Callback.GetList(response);
                loader.Delete();
            },
            function(response){
                if (response.status == 400) {
                    response = JSON.parse(response.data);
                    $scope.Callback.GetList(response);
                }
                loader.Delete();
            });
    }

    function ClickReset(){
        var container = $('#' + $scope.listId);
        var loader = new Loader(container);
        loader.Create();
        $http({
            url: $scope.GetBaseQuery() + '/filter',
            method: 'GET',
            data: {reset: true}
        }).then(
            function(response){
                response = JSON.parse(response.data);
                $scope.Callback.GetList(response);
                loader.Delete();
            },
            function(response){
                if (response.status == 400) {
                    response = JSON.parse(response.data);
                    $scope.Callback.GetList(response);
                }
                loader.Delete();
            });
    }

    function CallbackGetList(response){
        $timeout(function() {
            $scope.Get.Data(response);
        });
    }

    function ParseData(data){
        $scope.data = data;
        $scope.list = [];
        $scope.filters = DataProcessing.Normalize(data.filters);
        $scope._token.batch = data._token;
        $.each(data.list, function(i, obj){
            $scope.list.push(new $scope.model($scope, $http, $state, i, obj, SweetAlert, $filter));
        });
        $scope.pager = data.pager;

        $scope.Callback.GetData();
        $scope.ShowColumns();
        $scope.CheckUseFilter();
    }

    function ShowColumns(){
        var showInCookies = $cookieStore.get($scope.statelink.page);
        if(showInCookies)
            $scope.showColumns.all = showInCookies;
        else
            $scope.showColumns.all = angular.copy($scope.showDefaultColumns.all);
    }

    function CheckUseFilter(){
        var test = false;
        $.each($scope.filters, function(i){
            if($scope.filters[i].field && $scope.filters[i].field.hasOwnProperty('value') && $.trim($scope.filters[i].field.value) !== '') {
                test = true;
            }
        });

        if(test)
            showFilterForm();
    }

    function showFilterForm(){
        var form = $('#filter_form');
        var content = form.find('div.ibox-content').show();
        form.find('div.ibox-tools i:first').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        content.removeClass('border-bottom');
    }

    function ShowDateFilter(data){
        var showDate = false;
        if(data.from.value || data.to.value)
            showDate = true;

        return showDate;
    }

    function prepareToAutocomplete(field, prefix){
        field.template = FieldDispatcher.GetLayout('autocomplete');
        field.url = PiZoneConfig.apiPrefix + '/' + prefix + '/find?str=';
        if(field.attr && field.attr.model){
            field.init = field.attr.model;
        }
        field.onSelected = function(data){
            var value = '';
            if(data){
                value = data.originalObject.id;
            }
            field.value = value;
        };
        field.onFocusOut = function(){
            if(!field.value)
                $scope.$broadcast('angucomplete-alt:clearInput', field.id);
        };
    }

    function ViewNotify(action, res){
        function translate(key){
            return $filter('translate')(self.messages[action][res][key]);
        }
        SweetAlert.swal(translate('title'), translate('message'), res);
    }
}
ListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];