function Model($scope, $http, $state, i, obj, SweetAlert, $filter){
    var self = this;
    self.prefix = '';
    self.selected = false;
    self.statelink = {
        edit: ''
    };
    self.is_active = null;
    self._token = null;
    for(var key in obj){
        self[key] = obj[key];
    }
    self.Click = {
        Active: Active,
        Check: Check,
        Edit: Edit,
        Remove: Remove,
        SetDefault: ClickSetDefault,
        Move: Move
    };
    self.ViewNotify = ViewNotify;
    self.messages = {
        active: {
            success: {
                title: 'COMMON.ALERT.ACTIVE.SUCCESS.TITLE',
                message: 'COMMON.ALERT.ACTIVE.SUCCESS.MESSAGE'
            },
            warning: {
                title: 'COMMON.ALERT.ACTIVE.WARNING.TITLE',
                message: 'COMMON.ALERT.ACTIVE.WARNING.MESSAGE'
            },
            danger: {
                title: 'COMMON.ALERT.ACTIVE.DANGER.TITLE',
                message: 'COMMON.ALERT.ACTIVE.DANGER.MESSAGE'
            }
        },
        'default': {
            success: {
                title: 'COMMON.ALERT.DEFAULT.SUCCESS.TITLE',
                message: 'COMMON.ALERT.DEFAULT.SUCCESS.MESSAGE'
            },
            error: {
                title: 'COMMON.ALERT.DEFAULT.ERROR.TITLE',
                message: 'COMMON.ALERT.DEFAULT.ERROR.MESSAGE'
            }
        },
        remove: {
            confirm: {
                title: 'COMMON.ALERT.REMOVE.CONFIRM.TITLE',
                message: "COMMON.ALERT.REMOVE.CONFIRM.MESSAGE",
                confirmButton: "COMMON.ALERT.REMOVE.CONFIRM.CONFIRM_BUTTON",
                cancelButton: 'COMMON.ALERT.REMOVE.CONFIRM.CANCEL_BUTTON',
                cancelledTitle: 'COMMON.ALERT.REMOVE.CONFIRM.CANCELLED_TITLE',
                cancelledMessage: 'COMMON.ALERT.REMOVE.CONFIRM.CANCELLED_MESSAGE'
            },
            success: {
                title: 'COMMON.ALERT.REMOVE.SUCCESS.TITLE',
                message: 'COMMON.ALERT.REMOVE.SUCCESS.MESSAGE'
            },
            error: {
                title: 'COMMON.ALERT.REMOVE.ERROR.TITLE',
                message: 'COMMON.ALERT.REMOVE.ERROR.MESSAGE'
            }
        },
        move:{
            success: {
                title: 'COMMON.ALERT.MOVE.SUCCESS.TITLE',
                message: 'COMMON.ALERT.MOVE.SUCCESS.MESSAGE'
            },
            error: {
                title: 'COMMON.ALERT.ERROR.DANGER.TITLE',
                message: 'COMMON.ALERT.ERROR.DANGER.MESSAGE'
            }
        }
    };
    self.Callback = {
        Active: {
            Success: function(){},
            Warning: function(){},
            Error: function(){}
        },
        Delete: {
            Before: function(){},
            Success: function(){},
            Error: function(){}
        }
    };
    self.ConfirmDelete = ConfirmDelete;

    function Edit(){
        $state.go(self.statelink.edit, {id: self.id});
    }

    function Active(){
        var url = PiZoneConfig.apiPrefix + '/' + self.prefix + '/' + self.id + '/set/' + (self.is_active ? 'inactive' : 'active');
        $.ajax({
            url: url,
            dataType: 'json',
            success: function (message) {
                if (message.hasOwnProperty('active')) {
                    self.is_active = message.active;
                    $.each($scope.list, function (i) {
                        var item = $scope.list[i];
                        if(item.id == self.id)
                            item.is_active = message.active;
                    });
                }
                if (message.result == 'ok') {
                    if (self.is_active) {
                        self.Callback.Active.Success(message);
                        ViewNotify('active', 'success');
                    }
                    else {
                        self.Callback.Active.Warning(message);
                        ViewNotify('active', 'warning');
                    }
                }
                else {
                    self.Callback.Active.Error(message);
                    ViewNotify('active', 'danger');
                }
            }
        });
    }

    function Check(field){
        var url = PiZoneConfig.apiPrefix + '/' + self.prefix + '/' + self.id + '/check/' + field + '/' + (self[field] ? 'inactive' : 'active');
        $.ajax({
            url: url,
            dataType: 'json',
            success: function (message) {
                if (message.hasOwnProperty('active')) {
                    self[field] = message.active;
                    $.each($scope.list, function (i) {
                        var item = $scope.list[i];
                        if(item.id == self.id)
                            item[field] = message.active;
                    });
                }
                if (message.result == 'ok') {
                    if (self[field]) {
                        self.Callback.Active.Success(message);
                        ViewNotify('active', 'success');
                    }
                    else {
                        self.Callback.Active.Warning(message);
                        ViewNotify('active', 'warning');
                    }
                }
                else {
                    self.Callback.Active.Error(message);
                    ViewNotify('active', 'danger');
                }
            }
        });
    }

    function ClickSetDefault(field){
        var url = PiZoneConfig.apiPrefix + '/' + self.prefix + '/' + self.id + '/set_default';
        $http({
            url: url,
            method: 'POST',
            data: {
                field: field,
                active: self[field] ? false : true
            }
        }).then(
            function(message){
                message = message.data;
                if (message.result == 'ok') {
                    $.each($scope.list, function (i) {
                        var item = $scope.list[i];
                        if(item.id == self.id)
                            item[field] = true;
                        else
                            item[field] = false;
                    });
                    ViewNotify('default', 'success');
                }
                else
                    ViewNotify('default', 'error');
            },
            function(message){
                message = message.data;
                if (message.status == 404)
                    self.messages.remove.error.message = $filter('translate')('COMMON.ALERT.REMOVE.ERROR.ERROR_404', {id: self.id});
                if (message.status == 400)
                    self.messages.remove.error.message = 'COMMON.ALERT.REMOVE.ERROR.ERROR_400';
                if (message.status == 500)
                    self.messages.remove.error.message = 'COMMON.ALERT.REMOVE.ERROR.ERROR_500';
                ViewNotify('default', 'error');
            });
    }

    function Remove(){
        function translate(key){
            return $filter('translate')(self.messages.remove.confirm[key]);
        }
        SweetAlert.swal({
                title: translate('title'),
                text: translate('message'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: translate('confirmButton'),
                cancelButtonText: translate('cancelButton'),
                closeOnConfirm: false,
                closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    self.ConfirmDelete();
                } else {
                    SweetAlert.swal(translate('cancelledTitle'), translate('cancelledMessage'), "error");
                }
            });
    }

    function ConfirmDelete() {
        var url = PiZoneConfig.apiPrefix + '/' + self.prefix + '/' + self.id + '/delete';

        $http({
            url: url,
            method: 'POST',
            data: {_token: self._token},
        }).then(
            function(message){
                message = message.data;
                if (message.result == 'ok') {
                    ViewNotify('remove', 'success');
                    self.Callback.Delete.Success();
                    EventDispatcher.DispatchEvent('update_page_' + self.prefix);
                }
                else {
                    self.Callback.Delete.Error();
                    ViewNotify('remove', 'error');
                }
            },
            function(message){
                message = message.data;
                if (message.status == 404)
                    self.messages.remove.error.message = $filter('translate')('COMMON.ALERT.REMOVE.ERROR.ERROR_404', {id: self.id});
                if (message.status == 400)
                    self.messages.remove.error.message = 'COMMON.ALERT.REMOVE.ERROR.ERROR_400';
                if (message.status == 500)
                    self.messages.remove.error.message = 'COMMON.ALERT.REMOVE.ERROR.ERROR_500';
                self.Callback.Delete.Error();
                ViewNotify('remove', 'error');
            });
    }

    function Move($pos){
        var url = PiZoneConfig.apiPrefix + '/' + self.prefix + '/' + self.id + '/move/' + $pos;
        $.ajax({
            url: url,
            dataType: 'json',
            success: function (message) {
                if (message.result == 'ok') {
                    self.Callback.Active.Success(message);
                    ViewNotify('move', 'success');
                    EventDispatcher.DispatchEvent('update_page_' + self.prefix);
                }
                else {
                    self.Callback.Active.Error(message);
                    ViewNotify('move', 'error');
                }
            }
        });
    }

    function ViewNotify(action, res){
        function translate(key){
            return $filter('translate')(self.messages[action][res][key]);
        }
        SweetAlert.swal(translate('title'), translate('message'), res);
    }
}