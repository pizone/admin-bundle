function TranslationConfig($translateProvider) {
    $translateProvider
        .translations('en', {
            COMMON: enCommonPZTranslates
        })
        .translations('ru', {
            COMMON: ruCommonPZTranslates
        });

    $translateProvider.preferredLanguage('ru');

    $translateProvider.useCookieStorage();
    $translateProvider.storageKey('lang');
}
TranslationConfig.$inject = ['$translateProvider'];

angular
    .module('PiZone.Admin')
    .config(TranslationConfig);
