var DataProcessing = {
    Normalize: function(data){
        $.each(data, function(i, one){
            data[i].template = FieldDispatcher.GetLayout(one.type);
            // if(one.type == 'checkbox'){
            //     if(one.value == 1)
            //         data[i].value = true;
            //     else
            //         data[i].value = false;
            // }
            if(one.type == 'choice'){
                if(one.choices) {
                    if(one.value == 'null')
                        one.value = null;
                    var arr = $.grep(one.choices, function (n) {
                        if($.isArray(one.value)){
                            if($.inArray(n.attr, one.value) >=0)
                                return true;
                            return false;
                        }
                        else
                            return ( n.attr == one.value);
                    });

                    if (arr) {
                        if(arr.length > 1)
                            one.model = arr;
                        else
                            one.model = arr[0];
                    }
                    else
                        one.model = {};
                }
            }
            if(one.compound)
                data[i].form = DataProcessing.Normalize(data[i].form);
            if(one.type == 'collection'){
                var key;
                if(one.prototype) {
                    var collection = DataProcessing.Normalize(one.prototype);
                    for (key in one.prototype) {
                        collection[key].template = FieldDispatcher.GetLayout(one.prototype[key].type);
                    }
                    data[i].prototype = collection;
                }
                for(key in one.form){
                    data[i].form[key].form = DataProcessing.Normalize(one.form[key].form);
                }
            }
        });
        return data;
    }
};