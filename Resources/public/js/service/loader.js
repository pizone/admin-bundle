var Loader = function(idContainer, style){
    var self = this;
    self.content = idContainer;
    self.imgSrc = PiZoneConfig.pathToImg + 'ajax-loader.gif';
    self.loader = null;
    self.width = 0;
    self.height = 0;
    self.ready = false;
    self.Create = function(){
        self.ready = true;
        self.width = self.content.width() ? self.content.width() : '100%';
        self.height = self.content.height() ? self.content.height() : 32;
        self.content.hide();
        self.loader = self.GetHtmlLoader();
        self.content.after(self.loader);
    };
    self.GetHtmlLoader = function(){
        var box = $('<div></div>').addClass('loader ibox-content');
        //if(style)
        //    box.addClass(style);
        //var img = $('<img>').attr({src: self.imgSrc}).css({marginTop: (self.height/2)-16});
        box.html(self.GetLoader());
            //.width(self.width).height(self.height);
        return box;
    };
    self.Delete = function(){
        $('.loader').remove();
        self.content.show();
    };
    self.GetLoader = function(){
        var loader =
            '<div>' +
                '<div class="sk-spinner sk-spinner-fading-circle">' +
                    '<div class="sk-circle1 sk-circle"></div>' +
                    '<div class="sk-circle2 sk-circle"></div>' +
                    '<div class="sk-circle3 sk-circle"></div>' +
                    '<div class="sk-circle4 sk-circle"></div>' +
                    '<div class="sk-circle5 sk-circle"></div>' +
                    '<div class="sk-circle6 sk-circle"></div>' +
                    '<div class="sk-circle7 sk-circle"></div>' +
                    '<div class="sk-circle8 sk-circle"></div>' +
                    '<div class="sk-circle9 sk-circle"></div>' +
                    '<div class="sk-circle10 sk-circle"></div>' +
                    '<div class="sk-circle11 sk-circle"></div>' +
                    '<div class="sk-circle12 sk-circle"></div>' +
                '</div>' +
            '</div>';

        return loader;
    };
};

