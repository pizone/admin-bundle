angular
    .module('PiZone.Admin')
    .factory('MenuConfig', function () {
        var list = {},
        title = 'My site';

        var shop = {
            title: 'COMMON.SHOP',
            icon: 'fa fa-user',
            root: 'shop',
            url: null,
            role: null,
            children: [
                {
                    title: 'STOCK.LIST',
                    url: 'stock_list',
                    role: 'ROLE_ADMIN'
                },
                {
                    title: 'ORDER.LIST',
                    url: 'order_list',
                    role: 'ROLE_MANAGER'
                }
            ]
        };
        var content = {
            title: 'COMMON.LISTS',
            icon: 'fa fa-files-o',
            root: 'content',
            url: null,
            role: 'ROLE_ADMIN',
            children: [
                {
                    title: 'MENU.LIST',
                    url: 'menu_list',
                    role: null
                },
                {
                    title: 'SECTION.LIST',
                    url: 'section_list',
                    role: null
                },
                {
                    title: 'CONTENT.LIST',
                    url: 'content_list',
                    role: null
                },
                {
                    title: 'WEBITEM.LIST',
                    url: 'webitem_list',
                    role: null
                },
                {
                    title: 'LAYOUT.LIST',
                    url: 'layout_list',
                    role: null
                }
            ]
        };

        var catalog = {
            title: 'COMMON.CATALOG',
            icon: 'fa fa-list',
            root: 'catalog',
            url: null,
            role: 'ROLE_ADMIN',
            children: [
                {
                    title: 'CATEGORY.LIST',
                    url: 'category_list',
                    role: null
                },
                {
                    title: 'CATALOG.LIST',
                    url: 'catalog_list',
                    role: null
                },
                {
                    title: 'CCONTENT.LIST',
                    url: 'content_list',
                    role: null
                },
                {
                    title: 'GPARAM.LIST',
                    url: 'group_parameters_list',
                    role: null
                },
                {
                    title: 'TAG.LIST',
                    url: 'tag_list',
                    role: null
                },
                {
                    title: 'PROJECT.LIST',
                    url: 'project_list',
                    role: null
                }
            ]
        };

        var form = {
            title: 'COMMON.FORM',
            icon: 'fa fa-edit',
            root: 'form',
            url: null,
            role: null,
            children: [
                {
                    title: 'FFIELD.LIST',
                    url: 'field_list',
                    role: 'ROLE_ADMIN'
                },
                {
                    title: 'FSTATUS.LIST',
                    url: 'status_list',
                    role: 'ROLE_ADMIN'
                },
                {
                    title: 'FEVENT.LIST',
                    url: 'event_list',
                    role: 'ROLE_ADMIN'
                },
                {
                    title: 'FFORM.LIST',
                    url: 'form_list',
                    role: 'ROLE_MANAGER'
                }
            ]
        };

        var user = {
            title: 'USER.LIST',
            icon: 'fa fa-user',
            root: 'user',
            url: null,
            role: null,
            children: [
                {
                    title: 'USER.ADMIN_LIST',
                    url: 'default_user_list',
                    role: 'ROLE_ADMIN'
                },
                {
                    title: 'USER.BUYER_LIST',
                    url: 'catalog_user_list',
                    role: 'ROLE_MANAGER'
                }
            ]
        };

        addMenu(10, shop);
        addMenu(20, content);
        addMenu(30, catalog);
        addMenu(40, form);
        addMenu(50, user);


        function getMenu(){
            return sortMenu(list);
        }

        function addMenu(sort, item){
            list[sort] = item;
        }

        function sortMenu(obj){
            var keys = [];
            var sorted_obj = {};

            for(var key in obj){
                if(obj.hasOwnProperty(key)){
                    keys.push(key);
                }
            }

            keys.sort();

            for(var i = 0; i <= keys.length - 1; i++){
                sorted_obj[keys[i]] = obj[keys[i]];
            }

            return sorted_obj;
        }

        function getTitle(){
            return title;
        }

        function setTitle(title){
            title = title;
        }

        return {
            getMenu: getMenu,
            addMenu: addMenu,
            getTitle: getTitle,
            setTitle: setTitle
        };
    });