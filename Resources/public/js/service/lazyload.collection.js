var LazyLoadCollection = function(){
    var self = this,
        cssPluginPath = '/bundles/pizoneadmin/css/plugins/',
        jsPluginPath = '/bundles/pizoneadmin/js/plugins/'
        ;
    self.GetList = function(){
        return list;
    };
    self.GetEdit = function(){
        return edit;
    };

    function getFootable(){
        var str = 'footable/footable.';
        return {
            files: [
                jsPluginPath + str + 'all.min.js',
                cssPluginPath + str + 'core.css'
            ]
        }
    }
    function getNgFootable(){
        return {
            name: 'ui.footable',
            files: [
                jsPluginPath + 'footable/angular-footable.js'
            ]
        }
    }
    function getSweetalert(){
        var str = 'sweetalert/sweetalert.';
        return {
            files: [
                jsPluginPath + str + 'min.js',
                cssPluginPath + str + 'css'
            ]
        }
    }
    function getNgSweetalert(){
        return {
            name: 'oitozero.ngSweetAlert',
            files: [
                jsPluginPath + 'sweetalert/angular-sweetalert.min.js'
            ]
        }
    }
    function getNgSelect(){
        var str = 'ui-select/select.min.';
        return {
            name: 'ui.select',
            files: [
                jsPluginPath + str + 'js',
                cssPluginPath + str + 'css'
            ]
        }
    }
    function getNgSwitchery(){
        var str = 'switchery/switchery.';
        return {
            name: 'ui.switchery',
            files: [
                jsPluginPath + str + 'js',
                jsPluginPath + 'switchery/ng-switchery.js',
                cssPluginPath + str + 'css'
            ]
        }
    }
    function getDateTimePicker(){
        var str = 'bootstrap-datetimepicker';
        return {
            name: 'datetimepicker',
            files: [
                jsPluginPath + 'moment/moment.min.js',
                jsPluginPath + str + '/' + str + '.min.js',
                jsPluginPath + str + '/angular-' + str + '-directive.js'
            ]
        }
    }
    function getDatePicker(){
        var str = 'datepicker/angular-datepicker.';
        return {
            name: 'datePicker',
            files: [
                cssPluginPath + str + 'css',
                jsPluginPath + str +'js'
            ]
        }
    }
    function getClockPicker(){
        var str =  'clockpicker/clockpicker.';
        return {
            files: [
                cssPluginPath + str + 'css',
                jsPluginPath + str + 'js'
            ]
        }
    }
    function getSparkline(){
        return {
            files: [
                jsPluginPath + 'sparkline/jquery.sparkline.min.js'
            ]
        }
    }
    function getICheck(){
        return {
            files: [
                jsPluginPath + 'iCheck/icheck.min.js',
                cssPluginPath + 'iCheck/custom.css'
            ]
        }
    }
    function getChosen(){
        var str = 'chosen/chosen.';
        return {
            insertBefore: '#loadBefore',
            name: 'localytics.directives',
            files: [
                cssPluginPath + str + 'css',
                jsPluginPath + str + 'jquery.js',
                jsPluginPath + str + 'js'
            ]
        }
    }
    function getJasny(){
        return {
            files: [
                jsPluginPath + 'jasny/jasny-bootstrap.min.js'
            ]
        }
    }
    function getDropZone(){
        var str = 'dropzone/dropzone.';
        return {
            files: [
                jsPluginPath + str + 'js',
                cssPluginPath + str + 'css'
            ]
        }
    }
    function getTagsInput(){
        var str = 'ng-tags-input/ng-tags-input.';
        return {
            name: 'ngTagsInput',
            files: [
                cssPluginPath + str + 'min.css',
                jsPluginPath + str + 'js'
            ]
        }
    }

    var list = [
        getFootable(),
        getNgFootable(),
        getSweetalert(),
        getNgSweetalert(),
        getNgSelect(),
        getNgSwitchery(),
        getDateTimePicker(),
        getSparkline(),
        getICheck(),
        getChosen()
    ];
    var edit= [
        getFootable(),
        getNgFootable(),
        getSweetalert(),
        getNgSweetalert(),
        getNgSelect(),
        getNgSwitchery(),
        getICheck(),
        getClockPicker(),
        getJasny(),
        getDatePicker(),
        getDateTimePicker(),
        getChosen(),
        getDropZone(),
        getTagsInput()
    ]
};

