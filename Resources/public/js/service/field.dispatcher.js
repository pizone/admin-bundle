var FieldDispatcher = {
    path: PiZoneConfig.pathToTmpl + 'form/',
    GetLayout: function(type){
        switch (type){
            case 'text':
            case 'integer':
                return this.path + '_text_field.html';
            case 'file':
                return this.path + '_file_field.html';
            case 'textarea':
                return this.path + '_textarea_field.html';
            case 'hidden':
            case 'entity_hidden':
                return this.path + '_hidden_field.html';
            case 'checkbox':
                return this.path + '_checkbox_field.html';
            case 'choice':
                return this.path + '_choice_field.html';
            case 'simple_choice':
                return this.path + '_simple_choice_field.html';
            case 'multiselect':
                return this.path + '_multiselect_choice_field.html';
            case 'editor':
                return this.path + '_editor_field.html';
            case 'static':
                return this.path + '_static_field.html';
            case 'collection':
                return this.path + '_collection_field.html';
            case 'time':
                return this.path + '_time_field.html';
            case 'date':
                return this.path + '_date_field.html';
            case 'datetime':
                return this.path + '_datetime_field.html';
            case 'date_range':
                return this.path + '_date_range_field.html';
            case 'datetime_range':
                return this.path + '_datetime_range_field.html';
            case 'autocomplete':
                return this.path + '_autocomplete_field.html';
            case 'tags':
                return this.path + '_tags_field.html';
            case 'tags_autocomplete':
                return this.path + '_tags_autocomplete_field.html';
            case 'sms_text':
                return this.path + '_sms_text_field.html';
        }
    }
};