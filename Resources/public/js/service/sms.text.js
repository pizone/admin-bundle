function SmsText(field, translitField){
    var self = this;
    self.translitButton = 'Я=Ya';
    self.checkTranslit = false;
    self.tempTranslitText = '';

    self.Click = {
        Insert: ClickInsert,
        Translit: ClickTranslitSms
    };
    self.Get = {
        Length: GetLength,
        TranslitButton: GetTranslitButton,
        CheckTranslit: GetCheckTranslit
    };

    function GetCheckTranslit(){
        return self.checkTranslit;
    }

    function GetTranslitButton(){
        return self.translitButton;
    }

    function GetLength(newVal){
        var charachterLength = newVal.length,
            messageLength = 0,
            encoding = 'GSM_7BIT',
            perMessage = 160;

        if(charachterLength === 0)
            messageLength = 0;
        else {
            if (CheckSymbolsAscii(newVal) === true) {
                if (charachterLength > 160) {
                    messageLength = Math.ceil(charachterLength / 153);
                } else {
                    messageLength = 1;
                }
                encoding = 'GSM_7BIT';
                perMessage = 160;
            }
            else{
                if (charachterLength > 70) {
                    messageLength = Math.ceil(charachterLength / 67);
                } else {
                    messageLength = 1;
                }
                encoding = 'UTF16';
                perMessage = 70;
            }
        }

        return {
            char: charachterLength,
            message: messageLength,
            encoding: encoding,
            perMessage: perMessage
        };
    }

    function ClickInsert(code){
        field.value = (field.value ? field.value + ' ' : '') + GetInsertCode(code);
    }

    function ClickTranslitSms(callbackError) {
        var text = field.value;

        if (text) {
            field.value = tr(text);
            var translite_false = angular.copy(text);

            if (!self.checkTranslit) {
                self.checkTranslit = true;
                self.translitButton = 'Ya=Я';

                self.tempTranslitText = translite_false;

                translitField.value = true;
            } else {
                self.checkTranslit = false;
                self.translitButton = 'Я=Ya';

                field.value =  self.tempTranslitText;
                self.tempTranslitText = '';
                translitField.value = false;
            }
        } else
            if(callbackError)
                callbackError();
    }

    function CheckSymbolsAscii(str_data) {
        var i = 0,
            c1 = 0;

        str_data += '';
        while (i < str_data.length) {
            c1 = str_data.charCodeAt(i);
            if (c1 > 31 && c1 < 96) {
                i++;
            } else if (c1 > 96 && c1 < 127) {
                i++;
            } else if (c1 > 159 && c1 < 192) {
                i++;
            } else if (c1 == 10) {
                i++;
            } else {
                return false;
            }
        }
        return true;
    }

    function GetInsertCode(code){
        switch (code){
            case 'phone':
                return '#phone#';
            case 'first_name':
                return '#first-name#';
            case 'middle_name':
                return '#middle-name#';
            case 'last_name':
                return '#last-name#';
            case 'birth_date':
                return '#birthdate#';
            case 'gender':
                return '#gender#';
            case 'note1':
                return '#note1#';
            case 'note2':
                return '#note2#';
        }
    }

    function tr(s) {
        s = s.replace(/а/g, 'a');
        s = s.replace(/б/g, 'b');
        s = s.replace(/в/g, 'v');
        s = s.replace(/г/g, 'g');
        s = s.replace(/д/g, 'd');
        s = s.replace(/е/g, 'e');
        s = s.replace(/ё/g, 'e');
        s = s.replace(/ж/g, 'zh');
        s = s.replace(/з/g, 'z');
        s = s.replace(/и/g, 'i');
        s = s.replace(/й/g, 'y');
        s = s.replace(/к/g, 'k');
        s = s.replace(/л/g, 'l');
        s = s.replace(/м/g, 'm');
        s = s.replace(/н/g, 'n');
        s = s.replace(/о/g, 'o');
        s = s.replace(/п/g, 'p');
        s = s.replace(/р/g, 'r');
        s = s.replace(/с/g, 's');
        s = s.replace(/т/g, 't');
        s = s.replace(/у/g, 'u');
        s = s.replace(/ф/g, 'f');
        s = s.replace(/х/g, 'kh');
        s = s.replace(/ц/g, 'ts');
        s = s.replace(/ч/g, 'ch');
        s = s.replace(/ш/g, 'sh');
        s = s.replace(/щ/g, 'sch');
        s = s.replace(/ы/g, 'y');
        s = s.replace(/ь/g, "'");
        s = s.replace(/ъ/g, "'");
        s = s.replace(/э/g, 'e');
        s = s.replace(/ю/g, 'yu');
        s = s.replace(/я/g, 'ya');
        s = s.replace(/А/g, 'A');
        s = s.replace(/Б/g, 'B');
        s = s.replace(/В/g, 'V');
        s = s.replace(/Г/g, 'G');
        s = s.replace(/Д/g, 'D');
        s = s.replace(/Е/g, 'E');
        s = s.replace(/Ё/g, 'E');
        s = s.replace(/Ж/g, 'Zh');
        s = s.replace(/З/g, 'Z');
        s = s.replace(/И/g, 'I');
        s = s.replace(/Й/g, 'Y');
        s = s.replace(/К/g, 'K');
        s = s.replace(/Л/g, 'L');
        s = s.replace(/М/g, 'M');
        s = s.replace(/Н/g, 'N');
        s = s.replace(/О/g, 'O');
        s = s.replace(/П/g, 'P');
        s = s.replace(/Р/g, 'R');
        s = s.replace(/С/g, 'S');
        s = s.replace(/Т/g, 'T');
        s = s.replace(/У/g, 'U');
        s = s.replace(/Ф/g, 'F');
        s = s.replace(/Х/g, 'Kh');
        s = s.replace(/Ц/g, 'Ts');
        s = s.replace(/Ч/g, 'Ch');
        s = s.replace(/Ш/g, 'Sh');
        s = s.replace(/Щ/g, 'Sch');
        s = s.replace(/Ы/g, 'Y');
        s = s.replace(/Ь/g, "'");
        s = s.replace(/Ъ/g, "'");
        s = s.replace(/Э/g, 'E');
        s = s.replace(/Ю/g, 'Yu');
        s = s.replace(/Я/g, 'Ya');
        return s;
    }
}