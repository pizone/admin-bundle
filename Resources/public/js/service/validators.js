var Validators = function(){
    var self = this;
    self.test = true;

    self.isValid = function(){
        return self.test;
    };
    self.checkField = function(data){
        self.clearError(data.field);
        for(var i = 0; i <= data.assert.length-1; i++){
            self.Validator[data.assert[i].key](data.field, data.assert[i]);
        }
    };
    self.Validator = {
        simpleString: function(field, assert){
            if(field.value && field.value !== '')
                if(!/^[\S]+\S$/.test(field.value))
                    self.setError(field, assert.message);
        },
        string: function(field, assert){
            if(field.value && field.value !== ''){
                 if(!/^[а-яёa-zА-ЯЁA-Z0-9_\-\.\s]+$/.test(field.value))
                     self.setError(field, assert.message);
            }
        },
        notNull: function(field, assert){
            if(field.type == 'choice') {
                if(!field.model)
                    self.setError(field, assert.message);
            }
            else if(!field.value || field.value === '')
                self.setError(field, assert.message);
        },
        email: function(field, assert){
            if(field.value && field.value !== ''){
                if(!/^[-._a-zA-Z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/.test(field.value))
                    self.setError(field, assert.message);
            }
        },
        phone: function(field, assert){
            if(field.value && field.value !== ''){
                if(!/^[0-9]+$/.test(field.value)) {
                    self.setError(field, assert.message);
                }
            }
        },
        repeat: function(field, assert){
            if(field.value && field.value !== ''){
                if(field.value != assert.compare.value){
                    self.setError(field, assert.message);
                }
            }
        },
        regExp: function(field, assert){
            if(field.value && field.value !== '' && assert.pattern){
                var pattern = new RegExp(assert.pattern);
                if(!pattern.test(field.value))
                    self.setError(field, assert.message);
            }
        },
        int: function(field, assert){
            if(field.value && field.value !== ''){
                if(!/^[0-9]+$/.test(field.value)) {
                    self.setError(field, assert.message);
                }
            }
        },
        float: function(field, assert){
            if(field.value && field.value !== ''){
                if(!/^[-+]?(\d+[.])?\d+$/.test(field.value)) {
                    self.setError(field, assert.message);
                }
            }
        },
        minLength: function(field, assert){
            if(field.value && field.value !== ''){
                if(field.value.length < assert.minLength)
                    self.setError(field, assert.message);
            }
        },
        maxLength: function(field, assert){
            if(field.value && field.value !== ''){
                if(field.value.length > assert.maxLength)
                    self.setError(field, assert.message);
            }
        },
        minValue: function(field, assert){
            if(field.value && field.value !== ''){
                if(field.value < assert.minValue)
                    self.setError(field, assert.message);
            }
        },
        maxValue: function(field, assert){
            if(field.value && field.value !== ''){
                if(field.value > assert.maxValue)
                    self.setError(field, assert.message);
            }
        },
        compareMin: function(field, assert){
            if(field.value && field.value !== ''){
                if(assert.compare.value && assert.compare.value !== ''){
                    if(field.value > assert.compare.value)
                        self.setError(field, assert.message);
                }
            }
            else if(assert.compare.value && assert.compare.value !== ''){
                self.setError(field, assert.messageRequired);
            }
        },
        compareMax: function(field, assert){
            if(field.value && field.value !== ''){
                if(assert.compare.value && assert.compare.value !== ''){
                    if(field.value < assert.compare.value)
                        self.setError(field, assert.message);
                }
            }
            else if(assert.compare.value && assert.compare.value !== ''){
                self.setError(field, assert.messageRequired);
            }
        },
        unique: function(field, assert){
            var collection = {},
                forms = assert.collection();
            if(forms && Object.keys(forms).length > 1){
                $.each(forms, function(i){
                    $.each(forms[i], function(j) {
                        $.each(forms[i][j], function(k) {

                            if (forms[i][j][k].field && forms[i][j][k].field.name == assert.field && forms[i][j][k].field.value && forms[i][j][k].field.value !== '') {
                                if (collection[forms[i][j][k].field.value])
                                    self.setError(forms[i][j][k].field, assert.message);
                                collection[forms[i][j][k].field.value] = forms[i][j][k].field;
                            }
                        });
                    });
                });
            }
        },
        ip: function(field, assert){
            if(field.value && field.value !== ''){
                if(!/^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$/.test(field.value)) {
                    self.setError(field, assert.message);
                }
            }
        },
        sum: function(field, assert){
            var collection = 0,
                forms = assert.collection(),
                test = true;
            if (forms && Object.keys(forms).length > 1) {
                $.each(forms, function (i) {
                    $.each(forms[i], function (j) {
                        $.each(forms[i][j], function (k) {
                            if (forms[i][j][k].field.name == assert.field && forms[i][j][k].field.value && forms[i][j][k].field.value !== '') {
                                collection = collection + parseInt(forms[i][j][k].field.value);
                            }
                        });
                    });
                });

                if(assert.hasOwnProperty('max_value') && collection > assert.max_value)
                    test = false;
                if(assert.hasOwnProperty('min_value') && collection < assert.min_value)
                    test = false;
                if(assert.hasOwnProperty('equal') && collection != assert.equal)
                    test = false;

                if (!test) {
                    $.each(forms, function (i) {
                        $.each(forms[i], function (j) {
                            $.each(forms[i][j], function (k) {
                                if (forms[i][j][k].field.name == assert.field && forms[i][j][k].field.value && forms[i][j][k].field.value !== '') {
                                    self.setError(forms[i][j][k].field, assert.message);
                                }
                            });
                        });
                    });
                }
            }
        },
        file: function(field, assert){
            if(!$('#' + field.id).val())
                self.setError(field, assert.message);
        }
    };
    self.setError = function(field, message){
        self.test = false;
        field.valid = false;
        field.errors =[message];
    };
    self.clearError = function(field){
        field.valid = true;
        field.errors =[];
    };
};