var ScrollToBlock = {
    element: null,
    start: 0,
    change: 0,
    currentTime: 0,
    increment: 4,
    duration: 500,
    go: function(anchor){
        var location = 0;
        if (anchor.offsetParent) {
            do {
                location += anchor.offsetTop;
                anchor = anchor.offsetParent;
            } while (anchor);
        }
        location = location >= 0 ? location : 0;

        this.animate(location);
        return false;
    },
    animate: function (pos) {
        document.documentElement.scrollTop = 1;
        this.element = (document.documentElement && document.documentElement.scrollTop) ? document.documentElement : document.body;
        this.start = this.element.scrollTop;
        this.change = pos - this.start;
        this.step();
    },
    step: function(){
        var self = ScrollToBlock;
        self.currentTime += self.increment;
        var val = self.easeInOutQuad(self.currentTime, self.start, self.change, self.duration);
        self.element.scrollTop = val - 100;
        if (self.currentTime < self.duration) {
            setTimeout(self.step, self.increment);
        }
    },
    easeInOutQuad: function(t, b, c, d) {
        t /= d / 2;
        if (t < 1) return c / 2 * t * t + b;
        t--;
        return -c / 2 * (t * (t - 2) - 1) + b;
    }
};