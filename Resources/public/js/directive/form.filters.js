function FormFiltersIntis() {
    var controller = ['$scope', '$timeout', function ($scope, $timeout) {
        $scope.Click = {
            AddFilter: ClickAddFilter,
            HideFilter: ClickHideFilter,
            ShowAllFields: ClickShowAllFields,
            HideAllFields: ClickHideAllFields
        };

        function init() {
            console.log($scope.formFilters);

        }

        function ClickAddFilter(){

        }

        function ClickHideFilter(){

        }

        function ClickShowAllFields(){

        }

        function ClickHideAllFields(){

        }

        init();
    }];

    var template = '<div class="ibox float-e-margins">' +
        '<div class="ibox-title">' +
            '<h5>{{"COMMON.FILTER.TITLE" | translate }}</h5>' +
        '<div ibox-tools></div>' +
    '</div>' +
    '<div class="ibox-content">' +
        '<form class="form-horizontal" id="{{formFilterId}}" ng-submit="Click.Filters()">' +
            '<input type="{{_token.filter.type}}" id="{{_token.filter.id}}" name="{{_token.filter.full_name}}" value="{{_token.filter.value}}" ng-model="_token.filter.value"/>' +
            '<div ng-repeat="block in filters | filter: { show: true }">' +
                '<div class="form-group" ng-init="field = block.field" ng-class="{\'has-error\': !field.valid}">' +
                    '<div class="col-lg-10 text-left" ng-include="field.template"></div>' +
                        '<div class="col-lg-2 m-t-25 text-left">' +
                            '<div class="btn-group" ng-show="field.name != \'all\'">' +
                                '<button type="button" class="btn btn-warning btn-outline" ng-click="Click.HideFilter(block)">' +
                                '<i class="fa fa-minus"></i>' +
                                '</button>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="form-group">' +
                    '<div class="col-lg-10 text-left">' +
                        '<button class="btn btn-primary btn-outline" type="submit"><i class="fa fa-filter"></i> {{"COMMON.FILTER.APPLY" | translate}}</button>' +
                        '<button class="btn btn-default btn-outline" type="button" ng-click="Click.Reset()"><i class="fa fa-refresh"></i> {{"COMMON.FILTER.RESET" | translate}}</button>' +
                    '</div>' +
                    '<div dropdown dropdown-intis class="col-lg-2 text-left">' +
                        '<button type="button" class="btn btn-primary btn-outline dropdown-toggle" dropdown-toggle>' +
                            '<i class="fa fa-plus"></i>' +
                        '</button>' +
                        '<ul class="dropdown-menu" role="menu">' +
                            '<li ng-repeat="block in filters">' +
                                '<a ng-click="Click.AddFilter(block)">' +
                                    '<i class="fa fa-check" ng-if="block.show"></i>' +
                                    '<i class="fa-empty" ng-if="!block.show">&nbsp;</i>' +
                                    '{{block.field.label | translate}}' +
                                '</a>' +
                            '</li>' +
                            '<li class="divider"></li>' +
                            '<li>' +
                                '<a ng-click="Click.ShowAllFields()" class="dropdown-close"><i class="fa-empty">&nbsp;</i> {{"COMMON.FILTER.SHOW_ALL_FIELDS" | translate}}</a>' +
                            '</li>' +
                            '<li>' +
                                '<a ng-click="Click.HideAllFields()" class="dropdown-close"><i class="fa-empty">&nbsp;</i> {{"COMMON.FILTER.HIDE_ALL_FIELDS" | translate}}</a>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                '</div>' +
            '</form>' +
        '</div>' +
    '</div>';

    return {
        restrict: 'A',
        scope: {
            formFilters: '='
        },
        controller: controller,
        template: template
    };
}

angular
    .module('PiZone.Admin')
    .directive('formFilters', FormFiltersIntis);