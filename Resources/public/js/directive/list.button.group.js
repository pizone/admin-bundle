function ListButtonGroupIntis() {
    var controller = ['$scope', function ($scope) {
        function init() {
            $scope.model = angular.copy($scope.ngModel);
        }

        init();
    }];

    var template = '<div class="bs-component">' +
            '<div class="btn-group">' +
                '<button type="button" class="btn btn-xs btn-dark light" ng-click="model.Click.Edit()">' +
                    '<i class="fa fa-edit"></i>' +
                '</button>' +
                '<button type="button" class="btn btn-xs btn-danger" ng-click="model.Click.Remove()">' +
                    '<i class="fa fa-trash"></i>' +
                '</button>' +
            '</div>' +
        '</div>';
    return {
        restrict: 'A',
        scope: {
            ngModel: '=',
            listButtonGroupIntis: '='
        },
        controller: controller,
        template: template
    };
}

angular
    .module('PiZone.Admin')
    .directive('listButtonGroupIntis', ListButtonGroupIntis);