function IonRangeSlider() {
    return {
        restrict: 'A',
        scope: {
            rangeOptions: '='
        },
        link: function (scope, elem, attrs) {
            elem.ionRangeSlider(scope.rangeOptions);
        }
    };
}
angular
    .module('PiZone.Admin')
    .directive('ionRangeSlider', IonRangeSlider);