function DropdownIntis() {
    return function (scope, element) {
        element.on('click', function(e){
            if($(e.target).hasClass('dropdown-close')) {
                $('body').click();
                setTimeout(function(){
                    $('div.form-group.open').removeClass('open');
                }, 100);
            }
        });
        element.dropdown().on("hide.bs.dropdown", function(e) {
            if ($.contains(dropdown, e.target)) {
                e.preventDefault();
                setTimeout(function(){
                    $('div.form-group.open').removeClass('open');
                }, 100);
            }
        });
    };
}

angular
    .module('PiZone.Admin')
    .directive('dropdownIntis', DropdownIntis);