function LandingScrollspy(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.scrollspy({
                target: '.navbar-fixed-top',
                offset: 80
            });
        }
    };
}
angular
    .module('PiZone.Admin')
    .directive('landingScrollspy', LandingScrollspy);