function CheckboxField() {
    return {
        restrict: 'A',
        scope: {
            field: '=',
            block: '='
        },
        templateUrl: '/bundles/pizoneadmin/tmpl/form/_checkbox_field.html',
        controller: ['$scope', '$element', function ($scope, $element) {

        }]
    };
}
angular
    .module('PiZone.Admin')
    .directive('checkboxField', CheckboxField);