function FitHeight(){
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.css("height", $(window).height() + "px");
            element.css("min-height", $(window).height() + "px");
        }
    };
}
angular
    .module('PiZone.Admin')
    .directive('fitHeight', FitHeight);