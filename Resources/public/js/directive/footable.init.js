function FootableInitIntis($timeout, $state, $stateParams) {
    return function (scope, element, attr) {
        var footableTable = element.parents('table'),
            footableSort;

        function init() {
            if( !scope.$last ) {
                return false;
            }
            scope.$watch('showColumns.all', function() {
                if (footableTable.data('footable')) {
                    footableTable.data('footable').reset();
                    setHideColumns();
                    render();
                }
            }, true);

            scope.$evalAsync(function(){
                setHideColumns();
                render();
            });
        }

        function setHideColumns(){
            var showing = scope.$parent.showColumns;
            var th = footableTable.find('th');
            $.each(th, function(i){
                $(th[i]).removeData('hide');
                if($.inArray(i, showing.all) < 0)
                    $(th[i]).data("hide", "all");
            });
            $(th[th.length-1]).data("hide", "all");
        }

        function render(){
            if (! footableTable.hasClass('footable-loaded')) {
                footableTable.footable();
            }

            footableTable.trigger('footable_initialized');
            footableTable.trigger('footable_resize');
            footableTable.data('footable').redraw();

            footableSort = footableTable.data('footable-sort');
            if(!attr.sortOnList)
                footableSort.doSort = doSort;
            showSort();
        }

        function showSort(){
            var footable = footableSort.footable;

            var table = $(footable.table),
                sortClass = footable.options.classes.sort;

            if($stateParams.order_by && $stateParams.sort){
                $.each(scope.columns, function(i, one){
                    if($stateParams.sort == one.name){
                        var th = table.find("> thead > tr:last-child > th:eq(" + i + ")");

                        table.find("> thead > tr:last-child > th, > thead > tr:last-child > td").not(th).removeClass(sortClass.sorted + " " + sortClass.descending);

                        if($stateParams.order_by == 'asc')
                            th.removeClass(sortClass.descending).addClass(sortClass.sorted);
                        else
                            th.removeClass(sortClass.sorted).addClass(sortClass.descending);
                    }
                });
            }
        }

        function doSort(index, i) {
            var orderBy = $stateParams.order_by ? ($stateParams.order_by == 'asc' ? 'desc' : 'asc') : "desc";

            $state.go(scope.statelink.page, {order_by: orderBy, sort: scope.columns[index].name, pageId: $stateParams.pageId});
        }

        init();
    };
}
FootableInitIntis.$inject = ['$timeout', '$state', '$stateParams'];
angular
    .module('PiZone.Admin')
    .directive('footableInit', FootableInitIntis);