function ButtonActivePizone() {
    var template = '<button type="button" class="btn btn-xs btn-outline btn-success" ng-if="ngModel.is_active" ng-click="ngModel.Click.Active()">' +
            '<i class="fa fa-check"></i>' +
        '</button>' +
        '<button type="button" class="btn btn-xs btn-outline btn-warning" ng-if="!ngModel.is_active" ng-click="ngModel.Click.Active()">' +
            '<i class="fa fa-minus"></i>' +
        '</button>';
    return {
        restrict: 'A',
        scope: {
            ngModel: '='
        },
        template: template,
        controller: ['$scope', '$element', function ($scope, $element) {

        }]
    };
}

angular
    .module('PiZone.Admin')
    .directive('buttonActivePizone', ButtonActivePizone);