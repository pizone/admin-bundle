function Truncate($timeout){
    return {
        restrict: 'A',
        scope: {
            truncateOptions: '='
        },
        link: function(scope, element) {
            $timeout(function(){
                element.dotdotdot(scope.truncateOptions);

            });
        }
    };
}
Truncate.$inject = ['$timeout'];
angular
    .module('PiZone.Admin')
    .directive('truncate', Truncate);