function DatepickerIntis() {
    return function (scope, element) {
        element.datepicker({
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: 'dd.mm.yyyy'
        });
    };
}

angular
    .module('PiZone.Admin')
    .directive('datepickerIntis', DatepickerIntis);