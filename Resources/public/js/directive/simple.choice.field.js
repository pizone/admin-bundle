function SimpleChoiceField() {
    return {
        restrict: 'A',
        scope: {
            field: '=',
            block: '='
        },
        templateUrl: '/bundles/pizoneadmin/tmpl/form/_simple_choice_field.html',
        controller: ['$scope', '$element', function ($scope, $element) {

        }]
    };
}
angular
    .module('PiZone.Admin')
    .directive('simpleChoiceField', SimpleChoiceField);