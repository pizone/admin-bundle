function ChatSlimScroll($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element) {
            $timeout(function(){
                element.slimscroll({
                    height: '234px',
                    railOpacity: 0.4
                });

            });
        }
    };
}
ChatSlimScroll.$inject = ['$timeout'];
angular
    .module('PiZone.Admin')
    .directive('chatSlimScroll', ChatSlimScroll);