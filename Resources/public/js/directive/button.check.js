function ButtonCheckPizone() {
    var template = '<button type="button" class="btn btn-xs btn-outline btn-success" ng-if="ngModel[field]" ng-click="ngModel.Click.Check(field)">' +
        '<i class="fa fa-check"></i>' +
        '</button>' +
        '<button type="button" class="btn btn-xs btn-outline btn-warning" ng-if="!ngModel[field]" ng-click="ngModel.Click.Check(field)">' +
        '<i class="fa fa-minus"></i>' +
        '</button>';
    return {
        restrict: 'A',
        scope: {
            ngModel: '=',
            field: '='
        },
        template: template,
        controller: ['$scope', '$element', function ($scope, $element) {

        }]
    };
}

angular
    .module('PiZone.Admin')
    .directive('buttonCheckPizone', ButtonCheckPizone);