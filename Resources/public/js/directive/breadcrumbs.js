function BreadcrumbsIntis() {
    var controller = ['$scope', '$timeout', function ($scope, $timeout) {
        function init() {
            if(!$scope.breadcrumbs.active)
                $scope.breadcrumbs.active = $scope.breadcrumbs.title;
        }

        init();
    }];

    var template ='<div class="col-lg-10">' +
        '<h2>{{breadcrumbs.title | translate:{ param: breadcrumbs.param } }}</h2>' +
        '<ol class="breadcrumb">' +
            '<li>' +
                '<a href="index.html">{{"COMMON.LISTS" | translate:{ param: breadcrumbs.param } }}</a>' +
            '</li>' +
            '<li ng-repeat="item in breadcrumbs.path">' +
            '<a ui-sref="{{item.url}}(item.urlParam)">{{item.title | translate:{ param: item.param } }}</a>' +
            '</li>' +
            '<li class="active">' +
                '<strong>{{breadcrumbs.active | translate:{ param: breadcrumbs.param } }}</strong>' +
            '</li>' +
        '</ol>' +
    '</div>' +
    '<div class="col-lg-2"></div>';

    return {
        restrict: 'A',
        scope: {
            breadcrumbs: '='
        },
        controller: controller,
        template: template
    };
}

angular
    .module('PiZone.Admin')
    .directive('breadcrumbs', BreadcrumbsIntis);