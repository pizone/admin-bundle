function IboxFootableTools($timeout, $cookieStore) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: '/bundles/pizoneadmin/tmpl/common/ibox_footable_tools.html',
        controller: ['$scope', '$element', function ($scope, $element) {
            $scope.ShowHide = ShowHide;
            $scope.CloseBox = CloseBox;
            $scope.Click.ShowColumn = ClickShowColumn;
            $scope.Click.ShowDefaultColumns = ClickShowDefaultColumns;
            $scope.Click.ShowAllColumns = ClickShowAllColumns;

            function Init() {
                $scope.$watch('showColumns.all', function() {
                    CheckShowColumn();
                }, true);
            }

            function ClickShowColumn(index){
                $scope.columns[index].show = $scope.columns[index].show ? false : true;
                var position = $.inArray(index, $scope.showColumns.all);
                if(position < 0)
                    $scope.showColumns.all.push(index);
                else
                    $scope.showColumns.all.splice(position, 1);

                $cookieStore.put($scope.statelink.page, $scope.showColumns.all);
            }

            function ClickShowDefaultColumns(){
                $scope.showColumns.all = angular.copy($scope.showDefaultColumns.all);
                $cookieStore.put($scope.statelink.page, $scope.showColumns.all);
            }

            function ClickShowAllColumns(){
                $scope.showColumns.all = [];
                $.each($scope.columns, function(index){
                    $scope.showColumns.all.push(index);
                });

                $cookieStore.put($scope.statelink.page, $scope.showColumns.all);
            }

            function ShowHide() {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.find('div.ibox-content');
                content.slideToggle(200);

                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            }

            function CloseBox() {
                var ibox = $element.closest('div.ibox');
                ibox.remove();
            }

            function CheckShowColumn(){
                $.each($scope.columns, function(i){
                    if($.inArray(i, $scope.showColumns.all) >= 0)
                        $scope.columns[i].show = true;
                    else
                        $scope.columns[i].show = false;
                });
            }

            Init();
        }]
    };
}
IboxFootableTools.$inject = ['$timeout', '$cookieStore'];
angular
    .module('PiZone.Admin')
    .directive('iboxFootableTools', IboxFootableTools);