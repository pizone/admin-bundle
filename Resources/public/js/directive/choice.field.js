function ChoiceField() {
    return {
        restrict: 'A',
        scope: {
            field: '=',
            block: '='
        },
        templateUrl: '/bundles/pizoneadmin/tmpl/form/_choice_field.html',
        controller: ['$scope', '$element', function ($scope, $element) {

        }]
    };
}
angular
    .module('PiZone.Admin')
    .directive('choiceField', ChoiceField);