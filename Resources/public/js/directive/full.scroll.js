function FullScroll($timeout){
    return {
        restrict: 'A',
        link: function(scope, element) {
            $timeout(function(){
                element.slimscroll({
                    height: '100%',
                    railOpacity: 0.9
                });

            });
        }
    };
}
FullScroll.$inject = ['$timeout'];
angular
    .module('PiZone.Admin')
    .directive('fullScroll', FullScroll);