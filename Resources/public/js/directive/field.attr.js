function FieldAttr() {
    return {
        scope: { fieldAttr: '=' },
        restrict: 'A',
        controller: ['$scope', '$element', '$filter', function($scope, $element, $filter){
            $scope.$watch('fieldAttr', function(newVal){
                for(var attr in newVal){
                    $element.attr(attr,  $filter('translate')(newVal[attr]));
                }
            });
        }]
    };
}

angular
    .module('PiZone.Admin')
    .directive('fieldAttr', FieldAttr);