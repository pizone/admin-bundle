function PagingIntis() {
    var controller = ['$scope', '$timeout', function ($scope, $timeout) {
        function init() {
            $scope.pager = angular.copy($scope.pagingIntis);

            $scope.$watch('pagingIntis', function(newValue, oldValue) {
                if (newValue != oldValue){
                    $timeout(function() {
                        $scope.pager = newValue;
                    }, 0);
                }
            }, true);
        }

        init();
    }];

    var template ='<div class="pull-right" ng-if="pager.nb_pages > 1">' +
        '<nav>' +
        '<ul class="pagination pull-right">' +
        '<li ng-class="{false: \'disabled\'}[pager.has_previos_page]">' +
        '<a ng-click="pagingAction(1)"><i class="fa fa-angle-double-left"></i> </a>' +
        '</li>' +
        '<li class="prev" ng-class="{false: \'disabled\'}[pager.has_previos_page]">' +
        '<a ng-click="pagingAction(pager.previos_page)"><i class="fa fa-angle-left"></i> </a>' +
        '</li>' +
        '<li ng-if="pager.start > 1">' +
        '<span>...</span>' +
        '</li>' +
        '<li ng-repeat="page in pager.range" ng-class="{true: \'active\'}[pager.current_page == page]">' +
        '<a ng-click="pagingAction(page)">{{page}}</a>' +
        '</li>' +
        '<li ng-if="pager.end < pager.nb_pages">' +
        '<span>...</span>' +
        '</li>' +
        '<li class="next" ng-class="{false: \'disabled\'}[pager.has_next_page]">' +
        '<a ng-click="pagingAction(pager.next_page)"> <i class="fa fa-angle-right"></i></a>' +
        '</li>' +
        '<li ng-class="{false: \'disabled\'}[pager.has_next_page]">' +
        '<a ng-click="pagingAction(pager.nb_pages)"><i class="fa fa-angle-double-right"></i> </a>' +
        '</li>' +
        '</ul>' +
        '</nav>' +
        '</div>';

    return {
        restrict: 'A',
        scope: {
            pagingIntis: '=',
            pagingAction: '='
        },
        controller: controller,
        template: template
    };
}

angular
    .module('PiZone.Admin')
    .directive('pagingIntis', PagingIntis);