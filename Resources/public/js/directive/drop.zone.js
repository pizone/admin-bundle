function DropZone() {
    return {
        scope: {
            pzModel: '=',
            pzForm: '='
        },
        restrict: 'A',
        controller: ['$scope', '$element', '$filter', function($scope, $element, $filter){
            $element.dropzone({
                url: "/",
                maxFilesize: 100,
                paramName: $scope.pzModel.full_name,
                maxThumbnailFilesize: 5,
                uploadMultiple: false,
                addRemoveLinks: true,
                autoProcessQueue: false,
                init: function() {
                    if($scope.pzModel.info) {
                        var mockFile = {
                            name: $scope.pzModel.info.name,
                            size: $scope.pzModel.info.size,
                            type: $scope.pzModel.info.mimetype
                        };
                        this.addFile.call(this, mockFile);
                        this.options.thumbnail.call(this, mockFile, $scope.pzModel.info.path);
                    }

                    this.on('addedfile', function(file) {
                        if(!$scope.pzForm.has($scope.pzModel.full_name))
                            $scope.pzForm.append($scope.pzModel.full_name, file);
                        else
                            $scope.pzForm.set($scope.pzModel.full_name, file);

                        if(!$scope.pzForm.has($scope.pzModel.remove.full_name))
                            $scope.pzForm.append($scope.pzModel.remove.full_name, 'off');
                        else
                            $scope.pzForm.set($scope.pzModel.remove.full_name, 'off');
                    });
                    this.on('drop', function(file) {
                        if(!$scope.pzForm.has($scope.pzModel.full_name))
                            $scope.pzForm.append($scope.pzModel.full_name, file);
                        else
                            $scope.pzForm.set($scope.pzModel.full_name, file);

                        if(!$scope.pzForm.has($scope.pzModel.remove.full_name))
                            $scope.pzForm.append($scope.pzModel.remove.full_name, 'off');
                        else
                            $scope.pzForm.set($scope.pzModel.remove.full_name, 'off');
                    });
                    this.on('removedfile', function(file){
                        if(!$scope.pzForm.has($scope.pzModel.remove.full_name))
                            $scope.pzForm.append($scope.pzModel.remove.full_name, 'on');
                        else
                            $scope.pzForm.set($scope.pzModel.remove.full_name, 'on');
                    });
                }
            });
        }]
    };
}
angular
    .module('PiZone.Admin')
    .directive('dropZone', DropZone);