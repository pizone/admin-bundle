function clockPicker() {
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.clockpicker();
        }
    };
}
angular
    .module('PiZone.Admin')
    .directive('clockPicker', clockPicker);