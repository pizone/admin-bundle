function CheckboxPhoneBaseListField() {
    return {
        restrict: 'A',
        scope: {
            field: '=',
            block: '='
        },
        templateUrl: '/bundles/pizoneadmin/tmpl/form/_checkbox_phone_base_list_field.html',
        controller: ['$scope', '$element', function ($scope, $element) {

        }]
    };
}
angular
    .module('PiZone.Admin')
    .directive('checkboxPhoneBaseListField', CheckboxPhoneBaseListField);