function SlimScroll($timeout){
    return {
        restrict: 'A',
        scope: {
            boxHeight: '@'
        },
        link: function(scope, element) {
            $timeout(function(){
                element.slimscroll({
                    height: scope.boxHeight ? scope.boxHeight : '100%',
                    railOpacity: 0.9
                });

            });
        }
    };
}
SlimScroll.$inject = ['$timeout'];
angular
    .module('PiZone.Admin')
    .directive('slimScroll', SlimScroll);