function HiddenField() {
    return {
        restrict: 'A',
        scope: {
            field: '=',
            block: '='
        },
        templateUrl: '/bundles/pizoneadmin/tmpl/form/_hidden_field.html',
        controller: ['$scope', '$element', function ($scope, $element) {

        }]
    };
}
angular
    .module('PiZone.Admin')
    .directive('hiddenField', HiddenField);