function PzPermission() {
    return {
        scope: {role: '@', noRole: '@'},
        restrict: 'A',
        controller: ['$scope', '$element', 'SessionService', function ($scope, $element, SessionService) {
            if ($scope.role) {
                if (!SessionService.CheckCredentials($scope.role))
                    $element.hide();
            }
            if ($scope.noRole) {
                if (SessionService.CheckCredentials($scope.noRole))
                    $element.hide();
            }
        }]
    };
}

angular
    .module('PiZone.Admin')
    .directive('pzPermission', PzPermission);