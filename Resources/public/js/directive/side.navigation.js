function SideNavigation($timeout, $http, SessionService, MenuConfig) {
    return {
        restrict: 'A',
        link: function($scope, element) {
            $scope.items = MenuConfig.getMenu();
            $scope.title = MenuConfig.getTitle();
            $scope.logoutButtonId = 'logoutButtonId';
            $scope.username = '';
            $scope.avatar = PiZoneConfig.pathToImg + 'User-icon.png';
            $scope.showAdminLink = false;
            $scope.Click = {
                Logout: Logout
            };

            function Init(){
                GetInfo();
            }

            function GetInfo(){
                $http.get(PiZoneConfig.apiPrefix + '/account').success(function(data) {
                    $scope.username = data.username;
                    if(SessionService.CheckCredentials('ROLE_SUPER_ADMIN') || SessionService.CheckCredentials('ROLE_ADMIN'))
                        $scope.showAdminLink = true;
                    else
                        $scope.showAdminLink = false;
                });
            }

            function Logout(){
                var container = $('#' + $scope.logoutButtonId);
                var loader = new Loader(container);
                $.ajax({
                    url: PiZoneConfig.apiPrefix + '/logout',
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function(){
                        if(loader.ready === false)
                            loader.Create();
                    },
                    error: function(message) {
                        //PiZoneException.AjaxError(message, loader)
                    },
                    success: function(message){
                        loader.Delete();
                        if(message.result == 'ok') {
                            SessionService.ClearCredentials();
                            $scope.$state.go('login');
                        }
                        else{

                        }

                    }
                });
            }

            Init();



            // Call the metsiMenu plugin and plug it to sidebar navigation
            $timeout(function(){
                element.metisMenu();
            });
        }
    };
}
SideNavigation.$inject = ['$timeout', '$http','SessionService', 'MenuConfig'];
angular
    .module('PiZone.Admin')
    .directive('sideNavigation', SideNavigation);