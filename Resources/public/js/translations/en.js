var enCommonPZTranslates = {
    FILTER: {
        TITLE: 'Filters',
        SHOW_ALL_FIELDS: 'Show all fields',
        HIDE_ALL_FIELDS: 'Hide all fields',
        APPLY: 'Apply',
        RESET: 'Reset',
    },
    LOGIN: 'Login',
    LOGOUT: 'Logout',
    LISTS: 'Content',
    CATALOG: 'Catalog',
    SELECT_ALL: 'Checkboxes',
    SETTINGS: 'Settings',
    SUPPORT: 'Support',
    USER_AREA: 'User area',
    ADMINISTRATION: 'Administration',
    ACTIONS: 'Actions',
    NEW: 'Add new',
    SHOW_ON_PAGE: 'Show on page:',
    TO_LIST: 'To list',
    CLOSE: 'Close',
    CANCEL: 'Cancel',
    SAVE: 'Save',
    SUBMIT: 'Submit',
    SUBMIT_AND_LIST: 'Submit and to list',
    SUBMIT_AND_NEW: 'Submit and new',
    DELETE: 'Delete',
    SHOW_ALL_COLUMNS: 'Show all',
    SHOW_DEFAULT_COLUMNS: 'Show default',
    IMPORT_FROM_FILE: 'Import from file',
    BATCH: 'Batch actions',
    ACTIVE: 'Activation',
    INACTIVE: 'Deactivation',
    SHOP: 'Shop',
    FORM: 'Forms',
    ALERT: {
        SUBMIT: {
            SUCCESS: {
                TITLE: 'Saved!',
                MESSAGE: 'Object successfully saved!'
            },
            ERROR:{
                TITLE: 'Error!',
                MESSAGE: 'Correct the errors occurred when filling out the form and try to save it again.'
            }
        },
        DEFAULT: {
            SUCCESS: {
                TITLE: 'Saved!',
                MESSAGE: 'The object has been successfully installed by default!'
            },
            ERROR:{
                TITLE: 'Error!',
                MESSAGE: 'An error occurred while setting the default object.'
            }
        },
        REMOVE: {
            CONFIRM: {
                TITLE: "Are you sure?",
                MESSAGE: "Your will not be able to recover this!",
                CONFIRM_BUTTON: "Yes, delete it!",
                CANCEL_BUTTON: 'No, cancel!',
                CANCELLED_TITLE: 'Cancelled',
                CANCELLED_MESSAGE: 'The item is safe :)'
            },
            SUCCESS: {
                TITLE: 'Removed',
                MESSAGE: 'The object has been successfully removed!'
            },
            ERROR: {
                TITLE: 'Error!',
                MESSAGE: 'An error occurred while deleting the item!',
                ERROR_400: '400 Bad Request!',
                ERROR_404: 'Element with [id] = {{id}} does not exist!',
                ERROR_500: '500 Internal Server Error!'
            }
        },
        MOVE:{
            SUCCESS: {
                TITLE: 'Movement',
                MESSAGE: 'The object was successfully moved!'
            },
            ERROR:{
                TITLE: 'Movement',
                MESSAGE: 'Error moving object!'
            }
        },
        ACTIVE:{
            SUCCESS: {
                TITLE: 'Activation',
                MESSAGE: 'Item successfully activated!'
            },
            WARNING:{
                TITLE: 'Activation',
                MESSAGE: 'Item successfully deactivated!'
            },
            DANGER:{
                TITLE: 'Activation',
                MESSAGE: 'Error activated object!'
            }
        },
        BATCH: {
            REMOVE: {
                CONFIRM: {
                    TITLE: "Are you sure?",
                    MESSAGE: "Your will not be able to recover this!",
                    CONFIRM_BUTTON: "Yes, delete it!",
                    CANCEL_BUTTON: 'No, cancel!',
                    CANCELLED_TITLE: 'Cancelled',
                    CANCELLED_MESSAGE: 'The items is safe :)'
                },
                SUCCESS: {
                    TITLE: 'Removed',
                    MESSAGE: 'The objects has been successfully removed!'
                },
                ERROR: {
                    TITLE: 'Error!',
                    MESSAGE: 'An error occurred while deleting the items!',
                    ERROR_400: '400 Bad Request!',
                    ERROR_500: '500 Internal Server Error!'
                }
            },
            ACTIVE: {
                CONFIRM: {
                    TITLE: "Are you sure?",
                    MESSAGE: "Perform batch activation of the selected elements!",
                    CONFIRM_BUTTON: "Yes, activate it!",
                    CANCEL_BUTTON: 'No, cancel!',
                    CANCELLED_TITLE: 'Cancelled',
                    CANCELLED_MESSAGE: 'The items is safe :)'
                },
                SUCCESS: {
                    TITLE: 'Activated',
                    MESSAGE: 'The objects has been successfully activated!'
                },
                ERROR: {
                    TITLE: 'Error!',
                    MESSAGE: 'An error occurred while activated the items!',
                    ERROR_400: '400 Bad Request!',
                    ERROR_500: '500 Internal Server Error!'
                }
            },
            INACTIVE: {
                CONFIRM: {
                    TITLE: "Are you sure?",
                    MESSAGE: "Perform batch deactivation of the selected elements!",
                    CONFIRM_BUTTON: "Yes, deactivate it!",
                    CANCEL_BUTTON: 'No, cancel!',
                    CANCELLED_TITLE: 'Cancelled',
                    CANCELLED_MESSAGE: 'The items is safe :)'
                },
                SUCCESS: {
                    TITLE: 'Deactivated',
                    MESSAGE: 'The objects has been successfully deactivated!'
                },
                ERROR: {
                    TITLE: 'Error!',
                    MESSAGE: 'An error occurred while deactivated the items!',
                    ERROR_400: '400 Bad Request!',
                    ERROR_500: '500 Internal Server Error!'
                }
            }

        }
    },
    MESSAGE: {
        REQUIRED: {
            NOT_NULL: 'This field is required.'
        },
        REGEXP: {
            ERROR: 'Invalid data type.'
        }
    }
};