var ruCommonPZTranslates = {
    FILTER: {
        TITLE: 'Фильтры',
        SHOW_ALL_FIELDS: 'Показать все условия',
        HIDE_ALL_FIELDS: 'Скрыть все условия',
        APPLY: 'Применить',
        RESET: 'Очистить',
    },
    LOGIN: 'Логин',
    LOGOUT: 'Выйти',
    LISTS: 'Контент',
    CATALOG: 'Каталог',
    SELECT_ALL: 'Выбрать все',
    SETTINGS: 'Настройки',
    SUPPORT: 'Техподдержка',
    USER_AREA: 'Личный кабинет',
    ADMINISTRATION: 'Администрирование',
    ACTIONS: 'Действия',
    NEW: 'Новый контент',
    SHOW_ON_PAGE: 'Показывать на странице:',
    TO_LIST: 'К списку',
    CLOSE: 'Закрыть',
    CANCEL: 'Отменить',
    SAVE: 'Сохранить',
    SUBMIT: 'Сохранить',
    SUBMIT_AND_LIST: 'Сохранить и к списку',
    SUBMIT_AND_NEW: 'Сохранить и добавить',
    DELETE: 'Удалить',
    SHOW_ALL_COLUMNS: 'Показать все',
    SHOW_DEFAULT_COLUMNS: 'По умолчанию',
    IMPORT_FROM_FILE: 'Импортировать из файла',
    BATCH: 'Пакетное изменение',
    ACTIVE: 'Активировать',
    INACTIVE: 'Деактивировать',
    SHOP: 'Магазин',
    FORM: 'Формы',
    ALERT: {
        SUBMIT: {
            SUCCESS: {
                TITLE: 'Сохранено!',
                MESSAGE: 'Объект был успешно сохранен!'
            },
            ERROR:{
                TITLE: 'Ошибка!',
                MESSAGE: 'Исправьте ошибки возникшие при заполнении формы и попробуйте сохранить её еще раз.'
            }
        },
        DEFAULT: {
            SUCCESS: {
                TITLE: 'Сохранено!',
                MESSAGE: 'Объект был успешно установлен значением по умолчанию!'
            },
            ERROR:{
                TITLE: 'Ошибка!',
                MESSAGE: 'Произошла ошибка при попытке установить объект значением по умолчанию.'
            }
        },
        REMOVE: {
            CONFIRM: {
                TITLE: "Вы уверены?",
                MESSAGE: "Вы не сможете восстановить этого!",
                CONFIRM_BUTTON: "Да, удалить!",
                CANCEL_BUTTON: 'Нет, отменить!',
                CANCELLED_TITLE: 'Отменено',
                CANCELLED_MESSAGE: 'Элемент не тронут :)'
            },
            SUCCESS: {
                TITLE: 'Удаление',
                MESSAGE: 'Объект был успешно удален!'
            },
            ERROR: {
                TITLE: 'Ошибка!',
                MESSAGE: 'Произошла ошибка при удалении элемента!',
                ERROR_400: 'Неверные параметры запроса!',
                ERROR_404: 'Элемент с [id] = {{id}} не существует!',
                ERROR_500: 'Возникла непредвиденная ошибка сервера!'
            }
        },
        MOVE:{
            SUCCESS: {
                TITLE: 'Перемещение',
                MESSAGE: 'Объект был успешно перемещен!'
            },
            ERROR:{
                TITLE: 'Перемещение',
                MESSAGE: 'Ошибка перемещения объекта!'
            }
        },
        ACTIVE:{
            SUCCESS: {
                TITLE: 'Активация',
                MESSAGE: 'Объект успешно активирован!'
            },
            WARNING:{
                TITLE: 'Активация',
                MESSAGE: 'Объект успешно деактивирован!'
            },
            DANGER:{
                TITLE: 'Активация',
                MESSAGE: 'Ошибка активации объекта!'
            }
        },
        BATCH: {
            REMOVE: {
                CONFIRM: {
                    TITLE: "Вы уверены?",
                    MESSAGE: "Вы не сможете восстановить удаленные элементы!",
                    CONFIRM_BUTTON: "Да, удалить!",
                    CANCEL_BUTTON: 'Нет, отменить!',
                    CANCELLED_TITLE: 'Отменено',
                    CANCELLED_MESSAGE: 'Элементы не тронуты :)'
                },
                SUCCESS: {
                    TITLE: 'Удаление',
                    MESSAGE: 'Выбранные элементы были успешно удалены!'
                },
                ERROR: {
                    TITLE: 'Ошибка!',
                    MESSAGE: 'Произошла ошибка при удалении элементов!',
                    ERROR_400: '400 Bad Request!',
                    ERROR_500: '500 Internal Server Error!'
                }
            },
            ACTIVE: {
                CONFIRM: {
                    TITLE: "Вы уверены?",
                    MESSAGE: "Выполняется пакетная активация выбранных элементов!",
                    CONFIRM_BUTTON: "Да, активировать!",
                    CANCEL_BUTTON: 'Нет, отменить!',
                    CANCELLED_TITLE: 'Отменено',
                    CANCELLED_MESSAGE: 'Элементы не тронуты :)'
                },
                SUCCESS: {
                    TITLE: 'Активация',
                    MESSAGE: 'Выбранные элементы были успешно активированы!'
                },
                ERROR: {
                    TITLE: 'Ошибка!',
                    MESSAGE: 'Произошла ошибка при активации элементов!',
                    ERROR_400: '400 Bad Request!',
                    ERROR_500: '500 Internal Server Error!'
                }
            },
            INACTIVE: {
                CONFIRM: {
                    TITLE: "Вы уверены?",
                    MESSAGE: "Выполняется пакетная деактивация выбранных элементов!",
                    CONFIRM_BUTTON: "Да, деактивировать!",
                    CANCEL_BUTTON: 'Нет, отменить!',
                    CANCELLED_TITLE: 'Отменено',
                    CANCELLED_MESSAGE: 'Элементы не тронуты :)'
                },
                SUCCESS: {
                    TITLE: 'Деактивация',
                    MESSAGE: 'Выбранные элементы были успешно деактивированы!'
                },
                ERROR: {
                    TITLE: 'Ошибка!',
                    MESSAGE: 'Произошла ошибка при деактивации элементов!',
                    ERROR_400: '400 Bad Request!',
                    ERROR_500: '500 Internal Server Error!'
                }
            }
        }
    },
    MESSAGE: {
        REQUIRED: {
            NOT_NULL: 'Поле обязательно для заполнения.'
        },
        REGEXP: {
            ERROR: 'Неверный тип данных.'
        }
    }
};