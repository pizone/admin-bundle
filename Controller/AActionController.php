<?php

namespace PiZone\AdminBundle\Controller;

use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\Security\Csrf\CsrfToken;

class AActionController extends FOSRestController
{
    protected $model;
    protected $repository;
    protected $form;
    protected $route = array(
        'delete' => '',
        'list' => array(
            'name' => '',
            'parameters' => array()
        ),
        'batch' => array(
            'delete' => '',
            'active' => ''
        )
    );
    protected $manager = 'default';

    public function deleteAction(Request $request, $id)
    {
        $entity = $this->getObjectQuery($id)->getOneOrNullResult();

        if (!$entity) {
            $view = new View(json_encode(array('result' => 'error', 'message' => '404')), Codes::HTTP_NOT_FOUND);
            return $this->handleView($view);
        }

        try {
            if ('POST' == $request->getMethod()) {
                if(!$this->checkToken($request)){
                    $view = new View(json_encode(array('result' => 'error')), Codes::HTTP_BAD_REQUEST);
                    return $this->handleView($view);
                }

                $this->executeObjectDelete($entity);

                $view = new View(array('result' => 'ok'));
                return $this->handleView($view);
            }
        } catch (\Exception $e) {
            $view = new View(json_encode(array('result' => 'error', 'message' => '['.$e->getCode().'] ' . $e->getMessage())), Codes::HTTP_INTERNAL_SERVER_ERROR);
            return $this->handleView($view);
        }
    }

    public function setDefaultAction(Request $request, $id){
        $entity = $this->getObjectQuery($id)->getOneOrNullResult();

        if (!$entity) {
            $view = new View(json_encode(array('result' => 'error', 'message' => '404')), Codes::HTTP_NOT_FOUND);
            return $this->handleView($view);
        }

        try {
            if ('POST' == $request->getMethod()) {
                $field = $request->request->get('field');
                $active = $request->request->get('active');

                $em = $this->getDoctrine()->getManager($this->manager);
                $em->getRepository($this->repository)->setDefault($entity, $field, $active);

                $view = new View(array('result' => 'ok'));
                return $this->handleView($view);
            }
        } catch (\Exception $e) {
            $view = new View(json_encode(array('result' => 'error', 'message' => '['.$e->getCode().'] ' . $e->getMessage())), Codes::HTTP_INTERNAL_SERVER_ERROR);
            return $this->handleView($view);
        }
    }

    protected function checkToken($request){
        $intention = $request->getRequestUri();
        $token = $request->request->get('_token');

        $tokenManager = $this->get('security.csrf.token_manager');
        $token = new CsrfToken($intention, $token);
        if ($tokenManager->isTokenValid($token))
            return true;
        return false;
    }


    protected function executeObjectDelete($entity)
    {
        $em = $this->getDoctrine()->getManager($this->manager);
        $em->remove($entity);
        $em->flush();
        $em->clear();
    }

    protected function getObjectQuery($id)
    {
        return $this->getObjectQueryBuilder($id)->getQuery();
    }

    protected function getObjectQueryBuilder($id)
    {
        return $this->getDoctrine()
            ->getManager($this->manager)
            ->getRepository($this->repository)
            ->createQueryBuilder('q')
            ->where('q.id = :id')
            ->setParameter(':id', $id);
    }

    public function batchDeleteAction(Request $request){
        try {
            $ids = $request->request->get('ids');

            if ('POST' == $request->getMethod() && $ids) {
                if(!$this->checkToken($request)){
                    $view = new View(json_encode(array('result' => 'error')), Codes::HTTP_BAD_REQUEST);
                    return $this->handleView($view);
                }

                $this->executeBatchDelete($ids);

                $view = new View(array('result' => 'ok'));
                return $this->handleView($view);
            }
            else{
                $result = json_encode(array('result' => 'error'));
                $view = $this->view($result, Codes::HTTP_BAD_REQUEST)
                    ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
                return $this->handleView($view);
            }
        } catch (\Exception $e) {
            $result = json_encode(array('result' => 'error', 'message' => '['.$e->getCode().'] ' . $e->getMessage()));
            $view = $this->view($result, Codes::HTTP_BAD_REQUEST)
                ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
            return $this->handleView($view);
        }
    }

    public function batchActiveAction(Request $request){
        try {
            $ids = $request->request->get('ids');
            $active = $request->request->get('active');

            if ('POST' == $request->getMethod() && $ids) {
                if(!$this->checkToken($request)){
                    $view = new View(json_encode(array('result' => 'error')), Codes::HTTP_BAD_REQUEST);
                    return $this->handleView($view);
                }

                $this->executeBatchActive($ids, $active);

                $view = new View(array('result' => 'ok'));
                return $this->handleView($view);
            }
            else{
                $result = json_encode(array('result' => 'error'));
                $view = $this->view($result, Codes::HTTP_BAD_REQUEST)
                    ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
                return $this->handleView($view);
            }
        } catch (\Exception $e) {
            $result = json_encode(array('result' => 'error', 'message' => '['.$e->getCode().'] ' . $e->getMessage()));
            $view = $this->view($result, Codes::HTTP_BAD_REQUEST)
                ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
            return $this->handleView($view);
        }
    }

    protected function executeBatchDelete($selected)
    {
        $this->getDoctrine()->getManager($this->manager)
            ->createQuery('DELETE ' . $this->model . ' m WHERE m.id IN (:selected)')
            ->setParameter('selected', $selected)
            ->getResult();
    }

    protected function executeBatchActive($selected, $active)
    {
        $this->getDoctrine()->getManager($this->manager)
            ->createQuery('UPDATE ' . $this->model . ' m SET m.is_active = '. ($active ? 1:0) .' WHERE m.id IN (:selected)')
            ->setParameter('selected', $selected)
            ->getResult();
    }

    public function activeAction($id, $active)
    {
        $em = $this->getDoctrine()->getManager($this->manager);

        $entity = $em->getRepository($this->model)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WebItem entity.');
        }
        try {
            $test = $active == 'active' ? true : false;
            $entity->setIsActive($test);
            $em->flush();

            $result = array(
                'result' => 'ok',
                'active' => $entity->getIsActive()
            );
        } catch (\Exception $e) {
            $result = array(
                'result' => 'error',
                'message' => $e->getMessage(),
                'active' => $entity->getIsActive()
            );
        }

        $view = new View($result);
        return $this->handleView($view);
    }

    public function checkAction($id, $field, $active)
    {
        $em = $this->getDoctrine()->getManager($this->manager);

        $entity = $em->getRepository($this->model)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WebItem entity.');
        }
        try {
            $test = $active == 'active' ? true : false;
            $entity->set($field, $test);
            $em->flush();

            $result = array(
                'result' => 'ok',
                'active' => $entity->get($field)
            );
        } catch (\Exception $e) {
            $result = array(
                'result' => 'error',
                'message' => $e->getMessage(),
                'active' => $entity->get($field)
            );
        }

        $view = new View($result);
        return $this->handleView($view);
    }

    public function moveUpAction($id) {
        $repo = $this->getDoctrine()->getManager($this->manager)->getRepository($this->repository);
        $item = $repo->find($id);
        try {
            $t = $repo->moveUp($item, 1);
            if ($repo->verify() === true)
                $repo->recover();

            $result = array(
                'result' => 'ok'
            );
        } catch (\Exception $e) {
            $result = array(
                'result' => 'error',
                'message' => $e->getMessage()
            );
        }

        $view = new View($result);
        return $this->handleView($view);
    }

    public function moveDownAction($id) {
        $repo = $this->getDoctrine()->getManager($this->manager)->getRepository($this->repository);
        $item = $repo->find($id);
        try {
        $t = $repo->moveDown($item, 1);
        if ($repo->verify() === true)
            $repo->recover();

            $result = array(
                'result' => 'ok'
            );
        } catch (\Exception $e) {
            $result = array(
                'result' => 'error',
                'message' => $e->getMessage()
            );
        }

        $view = new View($result);
        return $this->handleView($view);
    }
}