<?php

namespace PiZone\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

interface IListController{
    public function indexAction(Request $request);
    public function filtersAction(Request $request);
    public function scopesAction();
}