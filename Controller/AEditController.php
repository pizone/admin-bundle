<?php

namespace PiZone\AdminBundle\Controller;

use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

class AEditController extends FOSRestController
{
    protected $model;
    protected $form;
    protected $routeList = array(
        'update' => '',
        'delete' => ''
    );
    protected $manager = 'default';

    /**
     * Displays a form to edit an existing WebItem entity.
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {
        $this->parseRequest($request);
        $em = $this->getDoctrine()->getManager($this->manager);

        $entity = $this->getObject($em, $id);

        if (!$entity) {
            $view = new View(array('result' => 'error'), Codes::HTTP_NOT_FOUND);
            return $this->handleView($view);
        }

        $editForm = $this->createEditForm($entity);

        $result =  json_encode($this->prepareView($id, $editForm));
        $view = $this->view($result)
            ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');

        return $this->handleView($view);
    }

    protected function parseRequest(Request $request){

    }


    /**
     * Edits an existing entity.
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, $id)
    {
        $this->parseRequest($request);
        $em = $this->getDoctrine()->getManager($this->manager);

        $entity = $this->getObject($em, $id);
        $this->preBindRequest($entity);
        if (!$entity) {
            $view = new View(array('result' => 'error'), Codes::HTTP_NOT_FOUND);
            return $this->handleView($view);
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $this->postBindRequest($entity, $em);

//        var_dump($editForm->getExtraData()); exit;
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->preSave($entity, $editForm);
            $this->save($em, $entity, $editForm);
            $this->postSave($entity);

            $result = json_encode(array('result' => 'ok'));
            $view = $this->view($result)
                ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
            return $this->handleView($view);
        }
        
        $result =  json_encode($this->prepareView($id, $editForm));
        $view = $this->view($result, Codes::HTTP_BAD_REQUEST)
            ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
        return $this->handleView($view);
    }

    protected function getObject($em, $id){
        return $em->getRepository($this->model)->find($id);
    }

    /**
     * Creates a form to edit entity.
     *
     * @param $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm($entity)
    {
        $form = $this->createForm($this->form, $entity);

        return $form;
    }

    protected function getDeleteFormToken($id){
        $tokenManager = $this->get('security.csrf.token_manager');
        $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $id));

        return $tokenManager->getToken($tokenId)->getValue();
    }

    public function prepareView($id, $editForm){
        $data = $this->get('pz_form')->formDataToArray($editForm->createView());
        return array(
            'action' => $this->generateUrl($this->routeList['update'], array('id' => $id)),
            'fields' => $data,
            '_delete_token' => $this->getDeleteFormToken($id)
        );
    }

    public function preBindRequest($entity){

    }

    public function postBindRequest($entity, $em){

    }

    public function preSave($entity, $editForm){

    }

    public function postSave($entity){

    }

    protected function save($em, $entity, $editForm){
        $em->persist($entity);
        $em->flush();
    }
}