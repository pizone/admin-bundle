<?php

namespace PiZone\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AdminController extends Controller
{
    public function indexAction()
    {
        return $this->render('PiZoneAdminBundle:Admin:index.html.twig');
    }

    public function getMainTemplateAction()
    {
        return $this->render('PiZoneAdminBundle:Admin:_main.html.twig');
    }

    public function getLoginTemplateAction()
    {
        return $this->render('PiZoneAdminBundle:Admin:_login.html.twig');
    }

    public function getRegisterTemplate(){
        return $this->render('PiZoneAdminBundle:Admin:_register.html.twig');
    }

    public function getContentTemplateAction()
    {
        return $this->render('PiZoneAdminBundle:Admin:_content.html.twig');
    }
}