<?php

namespace PiZone\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

interface IShowController{
    public function showAction(Request $request, $id);
}