<?php

namespace PiZone\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

interface IEditController{
    public function editAction(Request $request, $id);
    public function updateAction(Request $request, $id);
}