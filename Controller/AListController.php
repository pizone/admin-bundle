<?php

namespace PiZone\AdminBundle\Controller;

use PiZone\AdminBundle\Services\PiZonePager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\Yaml\Yaml;
use FOS\RestBundle\Util\Codes;

use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter as PagerAdapter;


/**
 * WebItem controller.
 *
 */
abstract class AListController extends FOSRestController
{
    protected $fieldList = array();
    protected $prefixSession = '';
    protected $routeList = array(
        'list' =>  array(
            'name' => 'pz_web_admin',
            'parameters' => array()
        ),
        'batch' => array(
            'delete' => '',
            'active' => ''
        )
    );
    protected $model = '';
    protected $repository = '';
    protected $filtersForm = '';
    protected $manager = 'default';

    /**
     * Lists all Layout entities.
     *
     */
    public function indexAction(Request $request)
    {
//        try {
            $this->parseRequest($request);
            $this->parseRequestForPager($request);

            $pager = $this->getPager();
            $form = $this->getFilterForm()->createView();

            $result = $this->prepareView($pager, $form);

            $view = $this->view(json_encode($result))
                ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');

            return $this->handleView($view);
//        }
//        catch (\Exception $e){
//            $len = strlen($this->prefixSession);
//            foreach($this->get('session')->all() as $key => $one){
//                if(substr($key, 0, $len) == $this->prefixSession)
//                    $this->get('session')->clear($key);
//            }
//            throw $e;
//        }
    }

    protected function getList($entities)
    {
        return $this->fieldList;
    }

    protected function prepareView($pager, $form){
        $tokenManager = $this->get('security.csrf.token_manager');
        $tokenDelId = $this->generateUrl($this->routeList['batch']['delete']);
        $tokenDel = $tokenManager->getToken($tokenDelId);
        $tokenActiveId = $this->generateUrl($this->routeList['batch']['active']);
        $tokenActive = $tokenManager->getToken($tokenActiveId);
        return array(
            'list' => $this->getList($pager),
            'pager' => $this->getPagination($pager),
            'filters' => $this->get('pz_form')->formDataToArray($form),
            '_token' => array(
                'remove' => $tokenDel->getValue(),
                'active' => $tokenActive->getValue()
            )
        );
    }

    protected function getPagination($pager)
    {

        $startEnd = PiZonePager::calculateStartAndEndPage($pager, 3);
        $result = array(
            'current_page' => $pager->getCurrentPage(),
            'nb_result' => $pager->getNbResults(),
            'nb_pages' => $pager->getNbPages(),
            'max_per_page' => $pager->getMaxPerPage(),
            'has_previos_page' => $pager->hasPreviousPage(),
            'has_next_page' => $pager->hasNextPage(),
            'range' => range($startEnd['start'], $startEnd['end']),
            'start' => $startEnd['start'],
            'end' => $startEnd['end']
        );

        if ($pager->hasPreviousPage())
            $result['previos_page'] = $pager->getPreviousPage();
        if ($pager->hasNextPage())
            $result['next_page'] = $pager->getNextPage();

        return $result;
    }

    protected function parseRequest(Request $request){

    }

    /**
     * Check if request contains pager parameters and
     * persist them in session if any.
     */
    protected function parseRequestForPager(Request $request)
    {
        if ($request->query->get('page')) {
            $this->setPage($request->query->get('page'));
        }

        if ($request->query->get('perPage')) {
            $this->setPerPage($request->query->get('perPage'));
        }

        if ($request->query->get('sort')) {
            $this->setSort($request->query->get('sort'), $request->query->get('order_by', 'ASC'));
        }
    }

    /**
     * Store in the session service the current page
     *
     * @param integer $page The page number
     */
    protected function setPage($page)
    {
        $this->get('session')->set($this->prefixSession . '\Page', $page);
    }

    /**
     * Return the stored page
     *
     * @return integer $page The page number
     */
    protected function getPage()
    {
        return $this->get('session')->get($this->prefixSession . '\Page', 1);
    }

    /**
     * Store in the session service the perPage
     *
     * @param integer $perPage The perPage number
     */
    protected function setPerPage($perPage)
    {
        $this->get('session')->set($this->prefixSession . '\PerPage', $perPage);
    }

    /**
     * Return the stored perPage
     *
     * @return integer $perPage The perPage number
     */
    protected function getPerPage()
    {
        return $this->get('session')->get($this->prefixSession . '\PerPage', 10);
    }

    protected function getPager()
    {
        $paginator = new Pagerfanta(new PagerAdapter($this->getQuery()));
        $paginator->setMaxPerPage($this->getPerPage());
        $paginator->setCurrentPage($this->getPage(), false, true);

        return $paginator;
    }

    /**
     * Store in the session service the current sort
     *
     * @param string $column The column
     * @param string $order_by The order sorting (ASC,DESC)
     */
    protected function setSort($column, $order_by)
    {
        $this->get('session')->set($this->prefixSession . '\Sort', $column);

        if ($order_by == 'desc') {
            $this->get('session')->set($this->prefixSession . '\OrderBy', 'DESC');
        } else {
            $this->get('session')->set($this->prefixSession . '\OrderBy', 'ASC');
        }
    }

    /**
     * Return the stored sort
     *
     * @return string The column to sort
     */
    protected function getSortColumn()
    {
        return $this->get('session')->get($this->prefixSession . '\Sort');
    }

    /**
     * Return the stored sort order
     *
     * @return string the order mode ASC|DESC
     */
    protected function getSortOrder()
    {
        return $this->get('session')->get($this->prefixSession . '\OrderBy', 'ASC');
    }

    public function filtersAction(Request $request)
    {
        $this->parseRequest($request);

        if ($request->get('reset')) {
            $this->setFilters(array());
            $this->get('session')->clear($this->prefixSession . '\Sort');
            $this->get('session')->clear($this->prefixSession . '\OrderBy');

            return new RedirectResponse($this->generateUrl($this->routeList['list']['name'], $this->routeList['list']['parameters']));
        }

        if ($request->getMethod() == "POST") {
            $form = $this->getFilterForm();
            $form->handleRequest($request);

            $filters = $form->getViewData();
        }

        if ($request->getMethod() == "GET") {
            $filters = $request->query->all();
        }

        if (isset($filters)) {
            $this->setFilters($filters);
        }

        return new RedirectResponse($this->generateUrl($this->routeList['list']['name'], $this->routeList['list']['parameters']));
    }

    /**
     * Store in the session service the current filters
     *
     * @param array the filters
     */
    protected function setFilters($filters)
    {
        $this->get('session')->set($this->prefixSession . '\Filters', $filters);
    }

    /**
     * Get filters from session
     */
    protected function getFilters()
    {
        $filters = $this->get('session')->get($this->prefixSession . '\Filters', array());


        return $filters;
    }

    public function scopesAction()
    {
        if ($this->get('request')->get('reset')) {
            $this->setScopes(array());

            return new RedirectResponse($this->generateUrl($this->routeList['list']['name'], $this->routeList['list']['parameters']));
        }

        $this->setScope($this->get('request')->get('group'), $this->get('request')->get('scope'));

        return new RedirectResponse($this->generateUrl($this->routeList['list']['name'], $this->routeList['list']['parameters']));
    }

    /**
     * Store in the session service the current scopes
     *
     * @param array the scopes
     */
    protected function setScopes($scopes)
    {
        $this->get('session')->set($this->prefixSession . '\Scopes', $scopes);
    }

    /**
     * Change the value of one Scope
     *
     * @param string the group name
     * @param string the scope name
     */
    protected function setScope($groupName, $scopeName)
    {
        $scopes = $this->getScopes();
        $scopes[$groupName] = $scopeName;
        $this->setScopes($scopes);
    }

    protected function getScopes()
    {
        return $this->get('session')->get($this->prefixSession . '\Scopes', $this->getDefaultScopes());
    }

    protected function getDefaultScopes()
    {
        $scopes = array();

        return $scopes;
    }

    /*
    * @return string|null the scope setted for the current group
    */
    protected function getScope($groupName)
    {
        $scopes = $this->getScopes();

        return isset($scopes[$groupName]) ? $scopes[$groupName] : null;
    }


    /**
     * Get additional parameters for rendering.
     *
     * return array
     */
    protected function getAdditionalRenderParameters()
    {
        return array();
    }

    /**
     * This function is for your convinience. Overwrite it if you need to
     * process the query.
     */
    protected function processQuery($query)
    {
        return $query;
    }

    protected function getQuery()
    {
        $query = $this->buildQuery();

        $this->processQuery($query);
        $this->processFilters($query);
        $this->processSort($query);
        $this->processScopes($query);

        return $query->getQuery();
    }

    protected function buildQuery()
    {
        return $this->getDoctrine()
            ->getManager($this->manager)
            ->getRepository($this->repository)
            ->createQueryBuilder('q');
    }

    protected function processSort($query)
    {
        if ($this->getSortColumn()) {
            if (!strstr($this->getSortColumn(), '.')) { //direct column
                $query->orderBy('q.' . $this->getSortColumn(), $this->getSortOrder());
            }
            else {
                $path = explode('.', $this->getSortColumn());
                $column = array_pop($path);

                $join = null;
                foreach($path as $i => $one){
                    if(!$join)
                        $join = 'q';
                    else
                        $join = $path[$i - 1];
                    if(!$this->checkJoin($join, $one, $query))
                        $this->addJoinFor($join, $one, $query);
                }

                $query->orderBy(array_pop($path).'.'.$column, $this->getSortOrder());
            }
        }
        else{
            $query->orderBy('q.id', 'DESC');
        }
    }

    protected function checkJoin($join, $table, $query){
        $joinDqlParts = $query->getDQLPart('join');
        $aliasAlreadyExists = false;

        foreach ($joinDqlParts as $joins) {
            foreach ($joins as $join) {
                if ($join->getAlias() === $table) {
                    $aliasAlreadyExists = true;
                    break 2;
                }
            }
        }

        return $aliasAlreadyExists;
    }

    protected function getFilterForm()
    {
        $filters = $this->getFilters();

        return $this->createForm($this->getFiltersType(), $filters);
    }

    protected function addJoinFor($join, $table, $query)
    {
        $query->leftJoin($join . '.' . $table, $table);
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);

        if (isset($filterObject['alias']) && null !== $filterObject['alias']) {
            $queryFilter->addStringFilter('alias', $filterObject['alias']);
        }
        if (isset($filterObject['description']) && null !== $filterObject['description']) {
            $queryFilter->addStringFilter('description', $filterObject['description']);
        }
        if (isset($filterObject['content']) && null !== $filterObject['content']) {
            $queryFilter->addStringFilter('content', $filterObject['content']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }

    }

    protected function processScopes($query)
    {
    }

    protected function getFiltersType()
    {
        $type = $this->filtersForm;

        return $type;
    }

    protected function getQueryFilter()
    {
        return $this->get('pz_filter');
    }

    public function verifyAction(){
        $result = $this->getDoctrine()->getManager($this->manager)->getRepository($this->repository)->verify();
        $view = $this->view(json_encode($result))
            ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');

        return $this->handleView($view);
    }

    public function recoverAction(){
        $result = $this->getDoctrine()->getManager($this->manager)->getRepository($this->repository)->recover();
        $view = $this->view(json_encode($result))
            ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');

        return $this->handleView($view);
    }

    public function reorderAction(){


        $em = $this->getDoctrine()->getManager($this->manager)->getRepository($this->repository);
        $node = $em->find(10);
        $result = $em->reorder($node, 'parent_id', 'ASC', false);
        $view = $this->view(json_encode($result))
            ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');

        return $this->handleView($view);
    }
}