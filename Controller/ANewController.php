<?php

namespace PiZone\AdminBundle\Controller;

use FOS\RestBundle\Util\Codes;
use PiZone\AccountBundle\Entity\Account;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;


class ANewController extends FOSRestController
{
    protected $model;
    protected $form;
    protected $route = array(
        'create' => ''
    );
    protected $manager = 'default';

    /**
     * Creates a new entity.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $this->parseRequest($request);
        $entity = $this->getObject();
        $this->preBindRequest($entity);
        $form = $this->createForm($this->form, $entity);
        $form->handleRequest($request);
        $this->postBindRequest($entity);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->preSave($entity, $form);
            $this->save($entity, $form);
            $this->postSave($entity);

            $result = $this->prepareResult($entity);
            $view = $this->view($result)
                ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
            return $this->handleView($view);
        }

        $result = $this->prepareView($form);
        $view = $this->view($result, Codes::HTTP_BAD_REQUEST)
            ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
        return $this->handleView($view);
    }

    protected function prepareResult($entity){
        return json_encode(array('result' => 'ok', 'id' => $entity->getId()));
    }

    protected function prepareView($form){
        $result = $this->get('pz_form')->formDataToArray($form->createView());
        $result = array(
            'action' => $this->generateUrl($this->route['create']),
            'fields' => $result
        );
        return $result;
    }


    protected function save($entity, $form){
        $em = $this->getDoctrine()->getManager($this->manager);
        $em->persist($entity);
        $em->flush();
    }

    protected function parseRequest(Request $request){

    }

    public function preBindRequest($entity){

    }

    public function postBindRequest($entity){

    }

    public function preSave($entity, $form){

    }

    public function postSave($entity){

    }

    protected function getObject(){
        return new $this->model();
    }

    protected function createCreateForm($entity)
    {
        $form = $this->createForm($this->form, $entity);

        return $form;
    }

    /**
     * Displays a form to create a new entity.
     *
     */
    public function newAction(Request $request)
    {
        $this->parseRequest($request);
        $entity = $this->getObject();
        $form = $this->createCreateForm($entity);

        $result = json_encode($this->prepareView($form));
        $view = $this->view($result)
            ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
        return $this->handleView($view);
    }
}
