<?php

namespace PiZone\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

interface INewController{
    public function newAction(Request $request);
    public function createAction(Request $request);
}