<?php

namespace PiZone\AdminBundle\Controller;

use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

class AShowController extends FOSRestController
{
    protected $model;
    protected $form;
    protected $routeList = array(
        'delete' => ''
    );

    /**
     * Displays a form to edit an existing WebItem entity.
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, $id)
    {
        $this->parseRequest($request);
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getObject($em, $id);

        if (!$entity) {
            $view = new View(array('result' => 'error'), Codes::HTTP_NOT_FOUND);
            return $this->handleView($view);
        }

        $data = $this->getList($entity);

        $result =  json_encode($this->prepareView($id, $data));
        $view = new View($result);
        return $this->handleView($view);
    }

    public function getList($entity){
        $result = array(
            'id' => $entity->getId()
        );

        return $result;
    }

    protected function parseRequest(Request $request){

    }

    protected function getObject($em, $id){
        return $em->getRepository($this->model)->find($id);
    }

    protected function getDeleteFormToken($id)
    {
        if ($this->routeList['delete']){
            $tokenManager = $this->get('security.csrf.token_manager');
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $id));

            return $tokenManager->getToken($tokenId)->getValue();
        }
        return null;
    }

    protected function prepareView($id, $data){
        return array(
            'fields' => $data,
            '_delete_token' => $this->getDeleteFormToken($id)
        );
    }

}