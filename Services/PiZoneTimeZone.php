<?php

namespace PiZone\AdminBundle\Services;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Response;

class PiZoneTimeZone{
    function formatOffset( $offset )
    {
        $hours = $offset / 3600;
        $remainder = $offset % 3600;
        $sign = $hours > 0 ? '+' : '-';
        $hour = (int)abs( $hours );
        $minutes = (int)abs( $remainder / 60 );

        if( $hour == 0 AND $minutes == 0 ) {
            $sign = ' ';
        }

        return $sign . str_pad( $hour, 2, '0', STR_PAD_LEFT ) . ':' . str_pad( $minutes, 2, '0' );

    }

//$utc = new DateTimeZone( 'UTC' );
//$dt = new DateTime( 'now', $utc );
//
//echo '<select name="LOCAL" class="selectpicker" data-width="300px"  data-live-search="true">';
//foreach( DateTimeZone::listIdentifiers() as $tz ) {
//$current_tz = new DateTimeZone( $tz );
//$offset = $current_tz->getOffset( $dt );
//$transition = $current_tz->getTransitions( $dt->getTimestamp(), $dt->getTimestamp() );
//$abbr = $transition[ 0 ][ 'abbr' ];
//
//if( $arResult[ 'USER' ][ 'LOCAL_TIME' ] == $tz ) {
//$sel = ' selected';
//} else {
//    $sel = '';
//}
//echo '<option value="' . $tz . '"' . $sel . '>' . $tz . ' [' . $abbr . ' ' . formatOffset( $offset ) . ']</option>';
//}
}