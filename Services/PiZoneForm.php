<?php

namespace PiZone\AdminBundle\Services;

use Symfony\Component\Form\FormView;

class PiZoneForm{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        $this->container = $container;
    }

    public function formDataToArray(FormView $form){
        $result = array();
        foreach($form as $key => $one){
            $vars = $one->vars;
            $result[$key] = array(
                'name' => isset($vars['name']) ? $vars['name']: null,
                'full_name' => isset($vars['full_name']) ? $vars['full_name'] : null,
                'id' => isset($vars['id']) ? $vars['id'] : null,
                'label' => isset($vars['label']) ? $vars['label'] : null,
                'required' => isset($vars['required']) ? $vars['required'] : null,
                'value' => isset($vars['value']) ? $vars['value'] : null,
                'valid' => isset($vars['valid']) ? $vars['valid'] : null,
                'errors' => $this->getErrors($vars),
                'type' => $this->getType($vars),
                'compound' => isset($vars['compound']) ? $vars['compound'] : null,
                'attr' => (isset($vars['attr']) && $vars['attr']) ? $vars['attr'] : null
            );
//            if($vars['value']){
//                if(is_array($vars['value']) || is_object($vars['value'])){
//                    $serializer = $this->getSerializer();
//                    $result[$key]['value'] = json_decode($serializer->serialize($vars['value'], 'json'));
//                }
//            }
            if($result[$key]['type'] == 'checkbox')
                $result[$key]['checked'] = $vars['checked'];
            if($result[$key]['type'] == 'choice') {
                $result[$key] = $result[$key] + $this->getChoices($vars);
                $result[$key]['value'] = isset($vars['value']) && $vars['value'] != '' ? $vars['value'] : null;
            }
            if(isset($vars['compound'])) {
                $result[$key]['form'] = $this->formDataToArray($vars['form']);
            }
            if(isset($vars['prototype']))
                $result[$key]['prototype'] = $this->formDataToArray($vars['prototype']);
        }

        return $result;
    }

    protected function getSerializer()
    {
        return $this->container->get('fos_rest.serializer');
    }

    private function getErrors($vars){
        $errors = array();
        if(isset($vars['valid']) && !$vars['valid']) {
            foreach ($vars['errors'] as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $errors;
    }

    private function getType($vars){
        if(isset($vars['block_prefixes'])){
            if($vars['block_prefixes'][1] == 'text' && $vars['block_prefixes'][2] == 'textarea' )
                return $vars['block_prefixes'][2];
            else
                return $vars['block_prefixes'][1];
        }
        return null;
    }

    private function getChoices($vars){
        $result = array(
            'multiple' => $vars['multiple'],
            'expanded' => $vars['expanded'],
            'preferred_choices' => $vars['preferred_choices'],
            'placeholder' => $vars['placeholder'],
            'choice_translation_domain' => $vars['choice_translation_domain'],
//            'is_selected' => $vars['is_selected'],
            'placeholder_in_choices' => $vars['placeholder_in_choices'],
//            'empty_value' => $vars['empty_data'] ? $vars['empty_data'] : '',
            'empty_value_in_choices' => isset($vars['empty_value_in_choices']) ? $vars['empty_value_in_choices'] : ''
        );
        $result['choices'] = $this->getChoiceList($vars['choices']);

        return $result;
    }

    private function getChoiceList($choices){
        $result = array();
        foreach($choices as $choice){
            if(!isset($choice->choices)) {
                $result[] = array(
                    'label' => $choice->label,
                    'value' => intval($choice->value),
                    'attr' => $choice->value
                );
            }
            else{
                foreach($choice->choices as $one){
                    $result[] = array(
                        'label' => $one->label,
                        'value' => intval($one->value),
                        'attr' => $one->value,
                        'group' => $choice->label
                    );
                }
            }
        }

        return $result;
    }
}