<?php
 
namespace PiZone\AdminBundle\Form\DataTransformer;
 
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
 
class EntityToIdTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;
 
    /**
     * @var string
     */
    protected $class;

    protected $em;
 
    public function __construct(ManagerRegistry $objectManager, $class, $em)
    {
        $this->objectManager = $objectManager;
        $this->class = $class;
        $this->em = $em;
    }
 
    public function transform($entity)
    {
        if (null === $entity) {
            return;
        }
 
        return $entity->getId();
    }
 
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }
 
        $entity = $this->objectManager->getManager($this->em)
                       ->getRepository($this->class)
                       ->find($id);

        if (null === $entity) {
            throw new TransformationFailedException(sprintf(
                'An instance of "%s" with id "%s" does not exist!',
                $this->class,
                $id
            ));
        }

        return $entity;
    }
}
